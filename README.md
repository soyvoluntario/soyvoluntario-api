# Soy Voluntario - API

Este repositorio contiene la api del proyecto **Soy Voluntario**. Este proyecto
tiene como finalidad poner a disposición de la ciudadania una plataforma que
les permite coordinarse en las labores de rescate, apoyo y entrega de ayuda.

## Instrucciones de desarrollo

Por favor revisar el archivo [CONTRIBUTING.md](CONTRIBUTING.md) antes de
realizar cualquier tipo de contribución. Para levantar el servicio de forma
local utlizar el comando:

```
docker-compose up -d --build
```

Para ejecutar el servidor local:

```
docker-compose run api python manage.py runserver
```

Para correr las pruebas:

```
docker-compose run test-service py.test
docker-compose run test-service flake8 soyvoluntario tests
docker-compose run test-service pylint soyvoluntario
```
