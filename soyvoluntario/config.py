"""Configuration module."""
import os


class BaseConfig:  # pylint: disable=too-few-public-methods
    """Base configuration."""
    BCRYPT_LOG_ROUNDS = 4
    DEBUG = False
    TESTING = False
    TOKEN_EXPIRATION_DAYS = 30
    SECRET_KEY = os.environ.get('SECRET_KEY', 'totally secure')
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class DevelopmentConfig(BaseConfig):  # pylint: disable=too-few-public-methods
    """Development configuration."""
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL',
                                             'sqlite:////tmp/soyvoluntario.db')


class TestingConfig(BaseConfig):  # pylint: disable=too-few-public-methods
    """Testing configuration."""
    DEBUG = True
    TESTING = True
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_TEST_URL', 'sqlite://')


class ProductionConfig(BaseConfig):  # pylint: disable=too-few-public-methods
    """Production configuration."""
    BCRYPT_LOG_ROUNDS = 14
    TOKEN_EXPIRATION_DAYS = 1
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL')


SETTINGS = {
    'Development': DevelopmentConfig,
    'Testing': TestingConfig,
    'Production': ProductionConfig,
}
