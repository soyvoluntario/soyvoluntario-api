"""Models modules."""
# pylint: disable=too-many-instance-attributes
import datetime
import jwt
import pytz

from flask import current_app

from soyvoluntario import db, bcrypt


class User(db.Model):  # pylint: disable=too-few-public-methods
    """User model."""
    __tablename__ = 'users'
    # pylint: disable=no-member,invalid-name
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(128), nullable=False)
    email = db.Column(db.String(128), nullable=False, unique=True)
    password = db.Column(db.String(255), nullable=False)
    twitter_handle = db.Column(db.String(32))
    cellphone = db.Column(db.String(32))
    organized_trips = db.relationship('Trip', backref='organizer')
    active = db.Column(db.Boolean(), default=True, nullable=False)
    created_at = db.Column(db.DateTime, nullable=False)
    # pylint: enable=no-member,invalid-name

    def __init__(self, name, email,  # pylint: disable=too-many-arguments
                 password, twitter_handle=None, cellphone=None,
                 created_at=datetime.datetime.now(tz=pytz.utc)):
        self.name = name
        self.email = email
        self.password = bcrypt.generate_password_hash(
            password, current_app.config.get('BCRYPT_LOG_ROUNDS')).decode()
        self.twitter_handle = twitter_handle
        self.cellphone = cellphone
        self.created_at = created_at

    def __str__(self):
        return "%s" % self.name

    def change_password(self, password):
        """Change the user's password."""
        self.password = bcrypt.generate_password_hash(
            password, current_app.config.get('BCRYPT_LOG_ROUNDS')).decode()

    def encode_auth_token(self):
        """Create user encoded token."""
        payload = {
            'exp': datetime.datetime.now(tz=pytz.utc) +
            datetime.timedelta(
                days=current_app.config.get('TOKEN_EXPIRATION_DAYS')
            ),
            'iat': datetime.datetime.now(tz=pytz.utc),
            'sub': {
                'id': self.id,
            }
        }
        return jwt.encode(payload, current_app.config.get('SECRET_KEY'),
                          algorithm='HS256')

    @staticmethod
    def decode_auth_token(auth_token):
        """Decode auth token."""
        try:
            payload = jwt.decode(auth_token,
                                 current_app.config.get('SECRET_KEY'))
            return payload['sub']
        except jwt.ExpiredSignatureError:
            return 'Signature expired'
        except jwt.InvalidTokenError:
            return 'Invalid token'


class State(db.Model):  # pylint: disable=too-few-public-methods
    """State model."""
    __tablename__ = 'states'
    # pylint: disable=no-member,invalid-name
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    cagee = db.Column(db.Integer)
    name = db.Column(db.String(64), nullable=False)
    municipalities = db.relationship('Municipality', backref='state')
    # pylint: enable=no-member,invalid-name

    def __init__(self, name, cagee=None):
        self.name = name
        self.cagee = cagee

    def __str__(self):
        return "%s" % self.name


class Municipality(db.Model):  # pylint: disable=too-few-public-methods
    """Municipality model."""
    __tablename__ = 'municipalities'
    # pylint: disable=no-member,invalid-name
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    cagem = db.Column(db.Integer)
    name = db.Column(db.String(128), nullable=False)
    state_id = db.Column(db.Integer,
                         db.ForeignKey('states.id', name='fk_state_id'),
                         nullable=False)
    towns = db.relationship('Town', backref='municipality')
    trips = db.relationship('Trip', backref='municipality')
    # pylint: enable=no-member,invalid-name

    def __init__(self, name, state_id, cagem=None):
        self.name = name
        self.state_id = state_id
        self.cagem = cagem

    def __str__(self):
        return "%s" % self.name


class Town(db.Model):  # pylint: disable=too-few-public-methods
    """Town model."""
    __tablename__ = 'towns'
    # pylint: disable=no-member,invalid-name
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    clge = db.Column(db.Integer)
    name = db.Column(db.String(128), nullable=False)
    ratings = db.relationship('Rating', backref='town')
    municipality_id = db.Column(db.Integer,
                                db.ForeignKey('municipalities.id',
                                              name='fk_municipality_id'),
                                nullable=False)
    longitude = db.Column(db.String(16), nullable=False)
    latitude = db.Column(db.String(16), nullable=False)
    trips = db.relationship('Trip', backref='destination')
    # pylint: enable=no-member,invalid-name

    def __init__(self, name,  # pylint: disable=too-many-arguments
                 municipality_id, longitude, latitude, clge=None):
        self.name = name
        self.municipality_id = municipality_id
        self.longitude = longitude
        self.latitude = latitude
        self.clge = clge

    def __str__(self):
        return "%s" % self.name


# pylint: disable=no-member,invalid-name
participants = db.Table('participants',
                        db.Column('user_id', db.Integer,
                                  db.ForeignKey('users.id',
                                                name='fk_user_id'),
                                  ),
                        db.Column('trip_id', db.Integer,
                                  db.ForeignKey('trips.id',
                                                name='fk_trip_id'),
                                  )
                        )
# pylint: enable=no-member,invalid-name


class Trip(db.Model):  # pylint: disable=too-few-public-methods
    """Trip model."""
    __tablename__ = 'trips'
    # pylint: disable=no-member,invalid-name
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    organizer_id = db.Column(db.Integer,
                             db.ForeignKey('users.id', name='fk_organizer_id'),
                             nullable=False)
    municipality_id = db.Column(db.Integer,
                                db.ForeignKey('municipalities.id',
                                              name='fk_municipality_id'),
                                nullable=False)
    destination_id = db.Column(db.Integer,
                               db.ForeignKey('towns.id',
                                             name='fk_destination_id'),
                               nullable=False)
    size = db.Column(db.Integer, nullable=False)
    date = db.Column(db.Date, nullable=False)
    time = db.Column(db.Time)
    participants = db.relationship('User', secondary='participants',
                                   backref=db.backref('trips', lazy='dynamic'))
    information = db.Column(db.Text)
    active = db.Column(db.Boolean(), default=True, nullable=False)
    finished = db.Column(db.Boolean(), default=False, nullable=False)
    created_at = db.Column(db.DateTime, nullable=False)
    # pylint: enable=no-member,invalid-name

    def __init__(self, organizer_id,  # pylint: disable=too-many-arguments
                 municipality_id, destination_id, size, date, time=None,
                 information=None,
                 created_at=datetime.datetime.now(tz=pytz.utc)):
        self.organizer_id = organizer_id
        self.municipality_id = municipality_id
        self.destination_id = destination_id
        self.size = size
        self.date = date
        self.time = time
        self.information = information
        self.created_at = created_at

    def __str__(self):
        return "%s - %s" % (self.destination, self.id)


class Rating(db.Model):  # pylint: disable=too-few-public-methods
    """Ratings model."""
    __tablename__ = 'ratings'
    # pylint: disable=no-member,invalid-name
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    town_id = db.Column(db.Integer,
                        db.ForeignKey('towns.id', name='fk_town_id'),
                        nullable=False)
    trip_id = db.Column(db.Integer,
                        db.ForeignKey('trips.id', name='fk_trip_id'),
                        nullable=False)
    value = db.Column(db.Integer, nullable=False)
    information = db.Column(db.Text)
    # pylint: enable=no-member,invalid-name

    def __init__(self, town_id, trip_id, value, information=None):
        self.town_id = town_id
        self.trip_id = trip_id
        self.value = value
        self.information = information

    def __str__(self):
        return "%s - %s" % (self.town, self.value)
