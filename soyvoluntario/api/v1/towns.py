"""Towns endpoint."""
from flask import Blueprint, request

from soyvoluntario.models import Town
from soyvoluntario.api.utils import (response_200, response_404, response_422)

# pylint: disable=invalid-name
towns_blueprint = Blueprint('towns-v1', __name__)
# pylint: enable=invalid-name


@towns_blueprint.route('/towns', methods=['GET'])
def get_towns():
    """Return a list with all towns."""
    if not request.args.get('all'):
        page = request.args.get('page')
        page_size = request.args.get('size')
        try:
            page_size = int(page_size) if page_size else None
            page_size = page_size if page_size else 50
            page_size = page_size if 0 < page_size < 50 else 50
        except ValueError:
            response_object = {
                'status': 'fail',
                'message': 'Invalid page size',
            }
            return response_422(response_object)

        total = Town.query.count()
        try:
            page = int(page) if page else None
            page = page if page else 1
            page = -(-total // page_size) if page*page_size > total else page
            page = page if page > 0 else 1
        except ValueError:
            response_object = {
                'status': 'fail',
                'message': 'Invalid page number',
            }
            return response_422(response_object)
        towns = Town.query.order_by(Town.id.desc()).offset(
            (page-1)*page_size).limit(page_size).all()
        has_next = True if page*page_size < total else False
    else:
        towns = Town.query.order_by(Town.id.desc()).all()
    towns_list = []

    for town in towns:
        town_object = {
            'id': town.id,
            'clge': town.clge,
            'name': town.name,
            'municipalityId': town.municipality_id,
            'longitude': town.longitude,
            'latitude': town.latitude,
            'hasTrips': True if town.trips else False,
        }
        towns_list.append(town_object)

    response_object = {
        'status': 'success',
        'towns': towns_list,
    }

    if not request.args.get('all'):
        response_object['page'] = page
        response_object['pageSize'] = page_size
        response_object['hasNext'] = has_next

    return response_200(response_object)


@towns_blueprint.route('/towns/<town_id>', methods=['GET'])
def get_town(town_id):
    """Return a town."""
    try:
        town_id = int(town_id)
    except ValueError:
        response_object = {
            'status': 'fail',
            'message': 'Invalid id',
        }
        return response_422(response_object)
    town = Town.query.filter_by(id=town_id).first()

    if not town:
        response_object = {
            'status': 'fail',
            'message': 'Town not found',
        }
        return response_404(response_object)
    trips = []
    for trip in town.trips:
        trip_object = {
            'id': trip.id,
        }
        trips.append(trip_object)

    response_object = {
        'town': {
            'name': town.name,
            'id': town.id,
            'longitude': town.longitude,
            'latitude': town.latitude,
            'trips': trips
        },
        'status': 'success'
    }

    return response_200(response_object)


@towns_blueprint.route('/towns/<town_id>/trips', methods=['GET'])
def get_town_trips(town_id):
    """Return the town trips."""
    try:
        town_id = int(town_id)
    except ValueError:
        response_object = {
            'status': 'fail',
            'message': 'Invalid id',
        }
        return response_422(response_object)
    town = Town.query.filter_by(id=town_id).first()

    if not town:
        response_object = {
            'status': 'fail',
            'message': 'Town not found',
        }
        return response_404(response_object)
    trips = []
    for trip in town.trips:
        trip_object = {
            'id': trip.id,
            'active': trip.active,
            'finished': trip.finished,
            'date': trip.date.isoformat(),
            'time': trip.time.strftime('%H:%M:%S') if trip.time else None,
            'municipalityId': trip.destination_id,
            'organizerId': trip.organizer_id,
            'size': trip.size,
            'information': trip.information,
            'participantsNumber': len(trip.participants),
        }
        trips.append(trip_object)

    response_object = {
        'trips': trips,
        'status': 'success',
    }

    return response_200(response_object)
