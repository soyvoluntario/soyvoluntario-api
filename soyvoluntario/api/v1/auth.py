"""Auth endpoint."""
import re

from flask import Blueprint, request

from soyvoluntario import bcrypt
from soyvoluntario.models import User
from soyvoluntario.api.utils import (response_200, response_400, response_409,
                                     response_422)

auth_blueprint = Blueprint('auth-v1', __name__)  # pylint: disable=invalid-name


@auth_blueprint.route('/auth/login', methods=['POST'])
def login():
    post_data = request.get_json()

    if not post_data:
        response_object = {
            'status': 'fail',
            'message': 'Empty request',
        }
        return response_400(response_object)

    email = post_data.get('email')
    if not email:
        response_object = {
            'status': 'fail',
            'message': 'Empty email',
        }
        return response_400(response_object)
    elif not re.match(r"^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$",
                      email):
        response_object = {
            'status': 'fail',
            'message': 'Invalid email',
        }
        return response_422(response_object)

    password = post_data.get('password')
    if not password:
        response_object = {
            'status': 'fail',
            'message': 'Empty password',
        }
        return response_400(response_object)

    user = User.query.filter_by(email=email).first()
    if user and bcrypt.check_password_hash(user.password, password):
        auth_token = user.encode_auth_token().decode()
        response_object = {
            'status': 'success',
            'message': 'Successfully logged in',
            'auth_token': auth_token,
        }
        return response_200(response_object)

    response_object = {
        'status': 'fail',
        'message': 'Wrong email or password',
    }
    return response_409(response_object)
