"""Soy Voluntario API - Version 1."""
from flask import Blueprint

from soyvoluntario.api.utils import response_200


api_v1_blueprint = Blueprint('v1', __name__)  # pylint: disable=invalid-name


@api_v1_blueprint.route('/v1', methods=['GET'])
def api_version():
    """Return API version."""
    response_object = {
        'status': 'success',
        'message': 'v1.0.0',
    }
    return response_200(response_object)
