"""Profile endpoint."""
from flask import Blueprint, request

from soyvoluntario import db
from soyvoluntario.models import User
from soyvoluntario.api.utils import response_200, authenticate
# pylint: disable=invalid-name
me_blueprint = Blueprint('me-v1', __name__)
# pylint: enable=invalid-name


@me_blueprint.route('/me', methods=['GET'])
@authenticate()
def get_profile(token_data):
    user = User.query.filter_by(id=token_data['id']).first()
    organized_trips = [{'id': trip.id} for trip in user.organized_trips]
    trips = [{'id': trip.id} for trip in user.trips]
    user_object = {
        'id': user.id,
        'email': user.email,
        'cellphone': user.cellphone,
        'twitterHandle': user.twitter_handle,
        'organizedTrips': organized_trips,
        'trips': trips,
    }
    response_object = {
        'status': 'success',
        'profile': user_object,
    }
    return response_200(response_object)


@me_blueprint.route('/me/trips', methods=['GET'])
@authenticate()
def get_trips(token_data):
    user = User.query.filter_by(id=token_data['id']).first()
    trips_object = []

    for trip in user.trips:
        trip_object = {
            'id': trip.id,
            'municipalityId': trip.municipality.id,
            'destinationId': trip.destination_id,
            'organizerId': trip.organizer_id,
            'date': trip.date.isoformat(),
            'time': trip.time.strftime('%H:%M:%S') if trip.time else None,
            'size': trip.size,
            'participantsNumber': len(trip.participants),
            'active': trip.active,
            'finished': trip.finished,
            'createdAt': trip.created_at.isoformat(),
            'information': trip.information,
        }
        trips_object.append(trip_object)

    response_object = {
        'status': 'success',
        'trips': trips_object,
    }
    return response_200(response_object)


@me_blueprint.route('/me', methods=['PUT'])
@authenticate()
def update_profile(token_data):
    user = User.query.filter_by(id=token_data['id']).first()
    data = request.get_json()
    email = data.get('email')
    print(email)
    if email:
        user.email = email

    password = data.get('password')
    if password:
        user.change_password(password)

    cellphone = data.get('cellphone')
    if cellphone:
        user.cellphone = cellphone

    twitter_handle = data.get('twitterHandle')
    if twitter_handle:
        user.twitter_handle = twitter_handle

    db.session.add(user)  # pylint: disable=no-member
    db.session.commit()  # pylint: disable=no-member

    organized_trips = [{'id': trip.id} for trip in user.organized_trips]
    trips = [{'id': trip.id} for trip in user.trips]
    user_object = {
        'id': user.id,
        'email': user.email,
        'cellphone': user.cellphone,
        'twitterHandle': user.twitter_handle,
        'organizedTrips': organized_trips,
        'trips': trips,
    }

    response_object = {
        'status': 'success',
        'message': 'Profile updated',
        'profile': user_object,
    }

    return response_200(response_object)
