"""Trips endpoint."""
from dateutil.parser import parse
from flask import Blueprint, request

from soyvoluntario import db
from soyvoluntario.models import Municipality, Town, Trip, User
from soyvoluntario.api.utils import (response_200, response_201,
                                     response_400, response_403, response_404,
                                     response_409, response_422, authenticate)

# pylint: disable=invalid-name
trips_blueprint = Blueprint('trips-v1', __name__)
# pylint: enable=invalid-name


@trips_blueprint.route('/trips', methods=['GET'])
def get_trips():
    if not request.args.get('all'):
        page = request.args.get('page')
        page_size = request.args.get('size')
        try:
            page_size = int(page_size) if page_size else None
            page_size = page_size if page_size else 50
            page_size = page_size if 0 < page_size < 50 else 50
        except ValueError:
            response_object = {
                'status': 'fail',
                'message': 'Invalid page size',
            }
            return response_422(response_object)

        total = Trip.query.count()
        try:
            page = int(page) if page else None
            page = page if page else 1
            page = -(-total // page_size) if page*page_size > total else page
            page = page if page > 0 else 1
        except ValueError:
            response_object = {
                'status': 'fail',
                'message': 'Invalid page number',
            }
            return response_422(response_object)
        trips = Trip.query.order_by(Trip.id.desc()).offset(
            (page-1)*page_size).limit(page_size).all()
        has_next = True if page*page_size < total else False
    else:
        trips = Trip.query.order_by(Trip.id.desc()).all()

    trips_object = []
    for trip in trips:
        trip_object = {
            'id': trip.id,
            'organizerId': trip.organizer_id,
            'municipalityId': trip.municipality_id,
            'destinationId': trip.destination.id,
            'size': trip.size,
            'date': trip.date.isoformat(),
            'time': trip.time.strftime('%H:%M:%S') if trip.time else None,
            'information': trip.information,
            'active': trip.active,
            'finished': trip.finished,
            'participantsNumber': len(trip.participants),
        }
        trips_object.append(trip_object)

    response_object = {
        'status': 'success',
        'trips': trips_object,
    }

    if not request.args.get('all'):
        response_object['page'] = page
        response_object['pageSize'] = page_size
        response_object['hasNext'] = has_next

    return response_200(response_object)


@trips_blueprint.route('/trips/<trip_id>', methods=['GET'])
def get_trip(trip_id):
    try:
        int(trip_id)
    except ValueError:
        response_object = {
            'status': 'fail',
            'message': 'Invalid id',
        }
        return response_422(response_object)
    trip = Trip.query.filter_by(id=trip_id).first()
    if trip:
        participants_object = [{'id': participant.id} for participant in
                               trip.participants]
        response_object = {
            'status': 'success',
            'trip': {
                'id': trip.id,
                'organizerId': trip.organizer.id,
                'municipalityId': trip.municipality.id,
                'destinationId': trip.destination.id,
                'size': trip.size,
                'date': trip.date.isoformat(),
                'time': trip.time.strftime('%H:%M:%S') if trip.time else None,
                'information': trip.information,
                'active': trip.active,
                'finished': trip.finished,
                'participants': participants_object,
            },
        }
        return response_200(response_object)
    response_object = {
        'status': 'fail',
        'message': 'Trip not found',
    }
    return response_404(response_object)


@trips_blueprint.route('/trips/<trip_id>/participants', methods=['GET'])
@authenticate(required=False)
def get_trip_participants(token_data, trip_id):
    try:
        int(trip_id)
    except ValueError:
        response_object = {
            'status': 'fail',
            'message': 'Invalid id',
        }
        return response_422(response_object)
    trip = Trip.query.filter_by(id=trip_id).first()
    user = None
    include_contact_info = False
    if token_data:
        user = User.query.filter_by(id=token_data['id']).first()
    if trip:
        if user and user in trip.participants:
            include_contact_info = True
        participants_object = []
        for participant in trip.participants:
            participant_object = {
                'id': participant.id,
                'name': participant.name,
            }
            if include_contact_info:
                contact_info = {
                    'cellphone': participant.cellphone,
                    'twitterHandle': participant.twitter_handle,
                }
                participant_object.update(contact_info)
            participants_object.append(participant_object)
        response_object = {
            'status': 'success',
            'participants': participants_object,
        }
        return response_200(response_object)

    response_object = {
        'status': 'fail',
        'message': 'Trip not found',
    }
    return response_404(response_object)


@trips_blueprint.route('/trips', methods=['POST'])
@authenticate()
def add_trip(token_data):
    """Post endpoint to add a trip."""
    post_data = request.get_json()

    if not post_data:
        response_object = {
            'status': 'fail',
            'message': 'Empty request',
        }
        return response_400(response_object)

    municipality_id = post_data.get('municipalityId')
    if not municipality_id:
        response_object = {
            'status': 'fail',
            'message': 'Empty municipality',
        }
        return response_400(response_object)
    elif not Municipality.query.filter_by(id=municipality_id).first():
        response_object = {
            'status': 'fail',
            'message': 'Invalid municipality id',
        }
        return response_422(response_object)

    destination_id = post_data.get('destinationId')
    if not destination_id:
        response_object = {
            'status': 'fail',
            'message': 'Empty destination',
        }
        return response_400(response_object)
    elif not Town.query.filter_by(id=destination_id).first():
        response_object = {
            'status': 'fail',
            'message': 'Invalid destination id',
        }
        return response_422(response_object)

    size = post_data.get('size')
    if not size:
        response_object = {
            'status': 'fail',
            'message': 'Empty size',
        }
        return response_400(response_object)
    try:
        int(size)
    except ValueError:
        response_object = {
            'status': 'fail',
            'message': 'Invalid size',
        }
        return response_422(response_object)

    date = post_data.get('date')
    if not date:
        response_object = {
            'status': 'fail',
            'message': 'Empty date',
        }
        return response_400(response_object)
    try:
        date = parse(date).date()
    except ValueError:
        response_object = {
            'status': 'fail',
            'message': 'Invalid date format',
        }
        return response_422(response_object)

    time = post_data.get('time')
    if time:
        try:
            time = parse(time).time()
        except ValueError:
            response_object = {
                'status': 'fail',
                'message': 'Invalid time format',
            }
            return response_422(response_object)

    information = post_data.get('information')

    trip = Trip(token_data['id'], municipality_id, destination_id, size, date,
                time, information)
    organizer = User.query.filter_by(id=token_data['id']).first()
    trip.participants.append(organizer)
    db.session.add(trip)  # pylint: disable=no-member
    db.session.commit()  # pylint: disable=no-member

    participants_object = [{'id': participant.id} for participant in
                           trip.participants]
    response_object = {
        'status': 'success',
        'message': 'Trip added',
        'trip': {
            'id': trip.id,
            'organizerId': trip.organizer.id,
            'municipalityId': trip.municipality.id,
            'destinationId': trip.destination.id,
            'size': trip.size,
            'date': trip.date.isoformat(),
            'time': trip.time.strftime('%H:%M:%S') if trip.time else None,
            'information': trip.information,
            'active': trip.active,
            'finished': trip.finished,
            'participants': participants_object,
        },
    }
    return response_201(response_object)


@trips_blueprint.route('/trips/<trip_id>', methods=['PUT'])
@authenticate()
def modify_trip(token_data, trip_id):
    """Post endpoint to add a trip."""
    trip = Trip.query.filter_by(id=trip_id).first()
    if not trip:
        response_object = {
            'status': 'fail',
            'message': 'Trip not found',
        }
        return response_404(response_object)
    if not trip.organizer.id == token_data['id']:
        response_object = {
            'status': 'error',
            'message': 'Unauthorized user',
        }
        return response_403(response_object)

    if trip.finished:
        response_object = {
            'status': 'fail',
            'message': 'Finished trips should not change'
        }
        return response_409(response_object)

    data = request.get_json()

    municipality_id = data.get('municipality_id')
    if municipality_id is not None:
        if not Municipality.query.filter_by(id=municipality_id).first():
            response_object = {
                'status': 'fail',
                'message': 'Invalid municipality id',
            }
            return response_422(response_object)
        trip.municipality_id = municipality_id

    destination_id = data.get('destination_id')
    if destination_id is not None:
        if not Town.query.filter_by(id=destination_id).first():
            response_object = {
                'status': 'fail',
                'message': 'Invalid destination id',
            }
            return response_422(response_object)
        trip.destination_id = destination_id

    size = data.get('size')
    if size is not None:
        try:
            int(size)
        except ValueError:
            response_object = {
                'status': 'fail',
                'message': 'Invalid size',
            }
            return response_422(response_object)
        if size < len(trip.participants):
            response_object = {
                'status': 'fail',
                'message': 'New size smaller than participants',
            }
            return response_409(response_object)
        trip.size = size

    date = data.get('date')
    if date is not None:
        try:
            date = parse(date).date()
        except ValueError:
            response_object = {
                'status': 'fail',
                'message': 'Invalid date format',
            }
            return response_422(response_object)
        trip.date = date

    time = data.get('time')
    if time is not None:
        try:
            time = parse(time).time()
        except ValueError:
            response_object = {
                'status': 'fail',
                'message': 'Invalid time format',
            }
            return response_422(response_object)
        trip.time = time

    information = data.get('information')
    if information is not None:
        trip.information = information

    active = data.get('active')
    if active is not None:
        trip.active = active

    finished = data.get('finished')
    if finished is not None:
        trip.finished = finished

    if data:
        db.session.add(trip)  # pylint: disable=no-member
        db.session.commit()  # pylint: disable=no-member

    participants_object = [{'id': participant.id} for participant in
                           trip.participants]
    response_object = {
        'status': 'success',
        'message': 'Trip modified',
        'trip': {
            'id': trip.id,
            'organizerId': trip.organizer.id,
            'municipalityId': trip.municipality.id,
            'destinationId': trip.destination.id,
            'size': trip.size,
            'date': trip.date.isoformat(),
            'time': trip.time.strftime('%H:%M:%S') if trip.time else None,
            'information': trip.information,
            'active': trip.active,
            'finished': trip.finished,
            'participants': participants_object,
        },
    }
    return response_200(response_object)


@trips_blueprint.route('/trips/<trip_id>/join', methods=['PUT'])
@authenticate()
def join_trip(token_data, trip_id):
    """Post endpoint to join a trip."""
    user_id = token_data['id']
    user = User.query.filter_by(id=user_id).first()
    trip = Trip.query.filter_by(id=trip_id).first()
    if user in trip.participants:
        data = {
            'status': 'success',
            'message': 'Joined the trip',
        }
        return response_200(data)
    elif len(trip.participants) < trip.size:
        trip.participants.append(user)
        db.session.add(trip)  # pylint: disable=no-member
        db.session.commit()  # pylint: disable=no-member
        data = {
            'status': 'success',
            'message': 'Joined the trip',
        }
        return response_200(data)
    data = {
        'status': 'fail',
        'message': 'The trip is full',
    }
    return response_409(data)


@trips_blueprint.route('/trips/<trip_id>/leave', methods=['PUT'])
@authenticate()
def leave_trip(token_data, trip_id):
    """Post endpoint to join a trip."""
    user_id = token_data['id']
    user = User.query.get(user_id)
    trip = Trip.query.filter_by(id=trip_id).first()
    if user not in trip.participants:
        data = {
            'status': 'success',
            'message': 'Left the trip',
        }
        return response_200(data)

    trip.participants.remove(user)
    db.session.add(trip)  # pylint: disable=no-member
    db.session.commit()  # pylint: disable=no-member
    data = {
        'status': 'success',
        'message': 'Left the trip',
    }
    return response_200(data)

    data = {
        'status': 'fail',
        'message': 'The trip is full',
    }

    return response_409(data)
