"""Users endpoint."""
import re

from flask import Blueprint, request

from soyvoluntario import db
from soyvoluntario.models import User
from soyvoluntario.api.utils import (response_200, response_201,
                                     response_400, response_404,
                                     response_409, response_422)

# pylint: disable=invalid-name
users_blueprint = Blueprint('users-v1', __name__)
# pylint: enable=invalid-name


@users_blueprint.route('/users', methods=['GET'])
def get_users():
    if not request.args.get('all'):
        page = request.args.get('page')
        page_size = request.args.get('size')
        try:
            page_size = int(page_size) if page_size else None
            page_size = page_size if page_size else 50
            page_size = page_size if 0 < page_size < 50 else 50
        except ValueError:
            response_object = {
                'status': 'fail',
                'message': 'Invalid page size',
            }
            return response_422(response_object)

        total = User.query.count()
        try:
            page = int(page) if page else None
            page = page if page else 1
            page = -(-total // page_size) if page*page_size > total else page
            page = page if page > 0 else 1
        except ValueError:
            response_object = {
                'status': 'fail',
                'message': 'Invalid page number',
            }
            return response_422(response_object)
        users = User.query.offset((page-1)*page_size).limit(page_size).all()
        has_next = True if page*page_size < total else False
    else:
        users = User.query.all()

    users_list = []
    for user in users:
        user_object = {
            'id': user.id,
            'name': user.name,
        }
        users_list.append(user_object)
    response_object = {
        'status': 'success',
        'users': users_list,
    }
    if not request.args.get('all'):
        response_object['page'] = page if page else 1
        response_object['pageSize'] = page_size if page_size else 50
        response_object['hasNext'] = has_next
    return response_200(response_object)


@users_blueprint.route('/users/<user_id>', methods=['GET'])
def get_user(user_id):
    try:
        user_id = int(user_id)
    except ValueError:
        response_object = {
            'status': 'fail',
            'message': 'Invalid id',
        }
        return response_422(response_object)
    user = User.query.filter_by(id=user_id).first()
    if not user:
        response_object = {
            'status': 'fail',
            'message': 'User not found',
        }
        return response_404(response_object)
    organized_trips = [{'id': trip.id} for trip in user.organized_trips]
    trips = [{'id': trip.id} for trip in user.trips]
    response_object = {
        'status': 'success',
        'user': {
            'id': user.id,
            'name': user.name,
            'organizedTrips': organized_trips,
            'trips': trips,
        }
    }
    return response_200(response_object)


@users_blueprint.route('/users/<user_id>/organizedtrips', methods=['GET'])
def get_user_organized_trips(user_id):
    try:
        user_id = int(user_id)
    except ValueError:
        response_object = {
            'status': 'fail',
            'message': 'Invalid id',
        }
        return response_422(response_object)
    user = User.query.filter_by(id=user_id).first()
    if not user:
        response_object = {
            'status': 'fail',
            'message': 'User not found',
        }
        return response_404(response_object)
    organized_trips = [{
        'id': trip.id,
        'active': trip.active,
        'finished': trip.finished,
        'date': trip.date.isoformat(),
        'time': trip.time.strftime('%H:%M:%S') if trip.time else None,
        'destinationId': trip.destination_id,
        'municipalityId': trip.municipality_id,
        'size': trip.size,
        'information': trip.information,
        'participantsNumber': len(trip.participants),
    } for trip in user.organized_trips]
    response_object = {
        'status': 'success',
        'organizedTrips': organized_trips,
    }
    return response_200(response_object)


@users_blueprint.route('/users/<user_id>/trips', methods=['GET'])
def get_user_trips(user_id):
    try:
        user_id = int(user_id)
    except ValueError:
        response_object = {
            'status': 'fail',
            'message': 'Invalid id',
        }
        return response_422(response_object)
    user = User.query.filter_by(id=user_id).first()
    if not user:
        response_object = {
            'status': 'fail',
            'message': 'User not found',
        }
        return response_404(response_object)
    trips = [{
        'id': trip.id,
        'active': trip.active,
        'finished': trip.finished,
        'date': trip.date.isoformat(),
        'time': trip.time.strftime('%H:%M:%S') if trip.time else None,
        'destinationId': trip.destination_id,
        'municipalityId': trip.municipality_id,
        'size': trip.size,
        'organizerId': trip.organizer_id,
        'information': trip.information,
        'participantsNumber': len(trip.participants),
    } for trip in user.trips]
    response_object = {
        'status': 'success',
        'trips': trips,
    }
    return response_200(response_object)


@users_blueprint.route('/users', methods=['POST'])
def add_user():
    post_data = request.get_json()

    if not post_data:
        response_object = {
            'status': 'fail',
            'message': 'Empty request',
        }
        return response_400(response_object)

    name = post_data.get('name')
    if not name:
        response_object = {
            'status': 'fail',
            'message': 'Empty name',
        }
        return response_400(response_object)

    email = post_data.get('email')
    if not email:
        response_object = {
            'status': 'fail',
            'message': 'Empty email',
        }
        return response_400(response_object)
    elif not re.match(r"^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$",
                      email):
        response_object = {
            'status': 'fail',
            'message': 'Invalid email',
        }
        return response_422(response_object)
    elif User.query.filter_by(email=email).first():
        response_object = {
            'status': 'fail',
            'message': 'Existing user',
        }
        return response_409(response_object)

    password = post_data.get('password')
    if not password:
        response_object = {
            'status': 'fail',
            'message': 'Empty password',
        }
        return response_400(response_object)

    twitter_handle = post_data.get('twitterHandle', None)
    cellphone = post_data.get('cellphone', None)

    user = User(name, email, password, twitter_handle, cellphone)
    db.session.add(user)  # pylint: disable=no-member
    db.session.commit()  # pylint: disable=no-member
    response_object = {
        'status': 'success',
        'message': '%s was added' % user.name,
        'profile': {
            'id': user.id,
            'name': user.name,
            'cellphone': user.cellphone,
            'twitterHandle': user.twitter_handle,
        }
    }
    return response_201(response_object)
