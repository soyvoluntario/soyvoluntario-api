"""Municipalities endpoint."""
from flask import Blueprint, request

from soyvoluntario.models import Municipality
from soyvoluntario.api.utils import (response_200, response_404, response_422)

# pylint: disable=invalid-name
municipalities_blueprint = Blueprint('municipalities-v1', __name__)
# pylint: enable=invalid-name


@municipalities_blueprint.route('/municipalities', methods=['GET'])
def get_municipalities():
    """Return a list with all municipalities."""
    if not request.args.get('all'):
        page = request.args.get('page')
        page_size = request.args.get('size')
        try:
            page_size = int(page_size) if page_size else None
            page_size = page_size if page_size else 50
            page_size = page_size if 0 < page_size < 50 else 50
        except ValueError:
            response_object = {
                'status': 'fail',
                'message': 'Invalid page size',
            }
            return response_422(response_object)

        total = Municipality.query.count()
        try:
            page = int(page) if page else None
            page = page if page else 1
            page = -(-total // page_size) if page*page_size > total else page
            page = page if page > 0 else 1
        except ValueError:
            response_object = {
                'status': 'fail',
                'message': 'Invalid page number',
            }
            return response_422(response_object)
        municipalities = Municipality.query.order_by(
            Municipality.id.desc()).offset((page-1)*page_size).limit(
            page_size).all()
        has_next = True if page*page_size < total else False
    else:
        municipalities = Municipality.query.order_by(
            Municipality.id.desc()).all()
    municipalities_list = []

    for municipality in municipalities:
        municipality_object = {
            'id': municipality.id,
            'cagem': municipality.cagem,
            'name': municipality.name,
            'hasTrips': True if municipality.trips else False,
            'stateId': municipality.state_id,
        }

        if request.args.get('with_trips'):
            municipality_object['trips'] = []
            for trip in municipality.trips:
                trip_object = {
                    'id': trip.id,
                    'active': trip.active,
                    'finished': trip.finished,
                }
                municipality_object['trips'].append(trip_object)

        municipalities_list.append(municipality_object)

    response_object = {
        'status': 'success',
        'municipalities': municipalities_list,
    }

    if not request.args.get('all'):
        response_object['page'] = page
        response_object['pageSize'] = page_size
        response_object['hasNext'] = has_next

    return response_200(response_object)


@municipalities_blueprint.route('/municipalities/<municipality_id>',
                                methods=['GET'])
def get_municipality(municipality_id):
    """Return a municipality."""
    try:
        municipality_id = int(municipality_id)
    except ValueError:
        response_object = {
            'status': 'fail',
            'message': 'Invalid id',
        }
        return response_422(response_object)
    municipality = Municipality.query.filter_by(id=municipality_id).first()

    if not municipality:
        response_object = {
            'status': 'fail',
            'message': 'Municipality not found',
        }
        return response_404(response_object)

    trips = []
    for trip in municipality.trips:
        trip_object = {
            'id': trip.id,
        }
        trips.append(trip_object)

    towns = []
    for town in municipality.towns:
        town_object = {
            'id': town.id,
        }
        towns.append(town_object)

    response_object = {
        'municipality': {
            'name': municipality.name,
            'id': municipality.id,
            'stateId': municipality.state_id,
            'cagem': municipality.cagem,
            'trips': trips,
            'towns': towns,
        },
        'status': 'success'
    }

    return response_200(response_object)


@municipalities_blueprint.route('/municipalities/<municipality_id>/towns',
                                methods=['GET'])
def get_municipality_towns(municipality_id):
    """Return the municipality towns."""
    try:
        municipality_id = int(municipality_id)
    except ValueError:
        response_object = {
            'status': 'fail',
            'message': 'Invalid id',
        }
        return response_422(response_object)
    municipality = Municipality.query.filter_by(id=municipality_id).first()

    if not municipality:
        response_object = {
            'status': 'fail',
            'message': 'Municipality not found',
        }
        return response_404(response_object)

    towns = []
    for town in municipality.towns:
        town_object = {
            'id': town.id,
            'name': town.name,
            'clge': town.clge,
            'longitude': town.longitude,
            'latitude': town.latitude,
            'hasTrips': True if town.trips else False,
        }
        towns.append(town_object)

    response_object = {
        'towns': towns,
        'status': 'success'
    }

    return response_200(response_object)
    return response_200(response_object)


@municipalities_blueprint.route('/municipalities/<municipality_id>/trips',
                                methods=['GET'])
def get_municipality_trips(municipality_id):
    """Return the municipality trips."""
    try:
        municipality_id = int(municipality_id)
    except ValueError:
        response_object = {
            'status': 'fail',
            'message': 'Invalid id',
        }
        return response_422(response_object)
    municipality = Municipality.query.filter_by(id=municipality_id).first()

    if not municipality:
        response_object = {
            'status': 'fail',
            'message': 'Municipality not found',
        }
        return response_404(response_object)

    trips = []
    for trip in municipality.trips:
        trip_object = {
            'id': trip.id,
            'active': trip.active,
            'finished': trip.finished,
            'date': trip.date.isoformat(),
            'time': trip.time.strftime('%H:%M:%S') if trip.time else None,
            'destinationId': trip.destination_id,
            'organizerId': trip.organizer_id,
            'size': trip.size,
            'information': trip.information,
            'participantsNumber': len(trip.participants),
        }
        trips.append(trip_object)

    response_object = {
        'trips': trips,
        'status': 'success'
    }

    return response_200(response_object)
