"""Towns endpoint."""
from flask import Blueprint, request

from soyvoluntario.models import State
from soyvoluntario.api.utils import (response_200, response_404, response_422)

# pylint: disable=invalid-name
states_blueprint = Blueprint('states-v1', __name__)
# pylint: enable=invalid-name


@states_blueprint.route('/states', methods=['GET'])
def get_states():
    """Return a list with all states."""
    if not request.args.get('all'):
        page = request.args.get('page')
        page_size = request.args.get('size')
        try:
            page_size = int(page_size) if page_size else None
            page_size = page_size if page_size else 50
            page_size = page_size if 0 < page_size < 50 else 50
        except ValueError:
            response_object = {
                'status': 'fail',
                'message': 'Invalid page size',
            }
            return response_422(response_object)

        total = State.query.count()
        try:
            page = int(page) if page else None
            page = page if page else 1
            page = -(-total // page_size) if page*page_size > total else page
            page = page if page > 0 else 1
        except ValueError:
            response_object = {
                'status': 'fail',
                'message': 'Invalid page number',
            }
            return response_422(response_object)
        states = State.query.offset((page-1)*page_size).limit(page_size).all()
        has_next = True if page*page_size < total else False
    else:
        states = State.query.all()
    states_list = []

    for state in states:
        state_object = {
            'id': state.id,
            'cagee': state.cagee,
            'name': state.name,
        }
        states_list.append(state_object)

    response_object = {
        'status': 'success',
        'states': states_list,
    }

    if not request.args.get('all'):
        response_object['page'] = page
        response_object['pageSize'] = page_size
        response_object['hasNext'] = has_next

    return response_200(response_object)


@states_blueprint.route('/states/<state_id>', methods=['GET'])
def get_state(state_id):
    """Return a state."""
    try:
        state_id = int(state_id)
    except ValueError:
        response_object = {
            'status': 'fail',
            'message': 'Invalid id',
        }
        return response_422(response_object)
    state = State.query.filter_by(id=state_id).first()

    if not state:
        response_object = {
            'status': 'fail',
            'message': 'State not found',
        }
        return response_404(response_object)
    municipalities = []
    for municipality in state.municipalities:
        municipality_object = {
            'id': municipality.id,
        }
        municipalities.append(municipality_object)

    response_object = {
        'state': {
            'name': state.name,
            'id': state.id,
            'cagee': state.cagee,
            'municipalities': municipalities,
        },
        'status': 'success'
    }

    return response_200(response_object)


@states_blueprint.route('/states/<state_id>/municipalities', methods=['GET'])
def get_state_municipalities(state_id):
    """Return the state municipalities."""
    try:
        state_id = int(state_id)
    except ValueError:
        response_object = {
            'status': 'fail',
            'message': 'Invalid id',
        }
        return response_422(response_object)
    state = State.query.filter_by(id=state_id).first()

    if not state:
        response_object = {
            'status': 'fail',
            'message': 'State not found',
        }
        return response_404(response_object)
    municipalities = []
    for municipality in state.municipalities:
        municipality_object = {
            'id': municipality.id,
            'name': municipality.name,
            'cagem': municipality.cagem,
        }
        municipalities.append(municipality_object)

    response_object = {
        'municipalities': municipalities,
        'status': 'success'
    }

    return response_200(response_object)
