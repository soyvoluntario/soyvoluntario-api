"""Utils methods for the api."""
from functools import wraps

from flask import jsonify, request

from soyvoluntario.models import User


def authenticate(required=True):
    def decorator(f):
        @wraps(f)
        def decorated_function(*args, **kwargs):
            response_object = {
                'status': 'error',
                'message': 'Provide a valid token',
            }
            auth_header = request.headers.get('Authorization')
            data = None
            if not auth_header:
                if required:
                    return jsonify(response_object), 401
            else:
                auth_token = auth_header.split(' ')[1]
                data = User.decode_auth_token(auth_token)
                if isinstance(data, str):
                    response_object['message'] = data
                    if required:
                        return jsonify(response_object), 401
            return f(data, *args, **kwargs)
        return decorated_function
    return decorator


def response_200(data):
    """Return json data with response code 200. Use for ok responses."""
    return jsonify(data), 200


def response_201(data):
    """Return json data with response code 201. Use for created objects."""
    return jsonify(data), 201


def response_400(data):
    """Return json data with response oode 400. Use for incomplete data."""
    return jsonify(data), 400


def response_403(data):
    """Return json data with response oode 403.
    Use for unauthorized requests."""
    return jsonify(data), 403


def response_404(data):
    """Return json data with response oode 404. Use for non existant data."""
    return jsonify(data), 404


def response_409(data):
    """Return json data with response oode 409. Use for conflicting data."""
    return jsonify(data), 409


def response_422(data):
    """Return json data with response oode 422. Use for invalid data."""
    return jsonify(data), 422
