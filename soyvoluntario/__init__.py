"""
Soy Voluntario module.

Here we initialize and create the app.
"""
import os

from flask import Flask, jsonify
from flask_bcrypt import Bcrypt
from flask_cors import CORS
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy

from soyvoluntario.config import SETTINGS

bcrypt = Bcrypt()  # pylint: disable=invalid-name
db = SQLAlchemy()  # pylint: disable=invalid-name
migrate = Migrate()  # pylint: disable=invalid-name


def create_app():
    """Create flask app."""
    app = Flask(__name__)

    app_settings = os.getenv('APP_SETTINGS', 'Development')
    settings_object = SETTINGS[app_settings]
    app.config.from_object(settings_object)

    CORS(app)

    bcrypt.init_app(app)
    db.init_app(app)
    migrate.init_app(app, db)

    from soyvoluntario.api.v1 import api_v1_blueprint
    from soyvoluntario.api.v1.auth import auth_blueprint
    from soyvoluntario.api.v1.states import states_blueprint
    from soyvoluntario.api.v1.towns import towns_blueprint
    from soyvoluntario.api.v1.municipalities import municipalities_blueprint
    from soyvoluntario.api.v1.trips import trips_blueprint
    from soyvoluntario.api.v1.users import users_blueprint
    from soyvoluntario.api.v1.me import me_blueprint

    app.register_blueprint(api_v1_blueprint, url_prefix='/api')
    app.register_blueprint(auth_blueprint, url_prefix='/api/v1')
    app.register_blueprint(states_blueprint, url_prefix='/api/v1')
    app.register_blueprint(municipalities_blueprint, url_prefix='/api/v1')
    app.register_blueprint(towns_blueprint, url_prefix='/api/v1')
    app.register_blueprint(trips_blueprint, url_prefix='/api/v1')
    app.register_blueprint(users_blueprint, url_prefix='/api/v1')
    app.register_blueprint(me_blueprint, url_prefix='/api/v1')

    @app.route('/api/ping')
    def pong():  # pylint: disable=unused-variable
        """Healt check"""
        response = jsonify({
            'status': 'success',
            'message': 'pong',
        })
        return response, 200

    return app
