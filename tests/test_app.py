import json

from tests.base import BaseTestCase


class TestApp(BaseTestCase):
    """Test the app is working."""

    def test_ping(self):
        """Ping the app to ensure it's running."""
        response = self.client.get('/api/ping')
        data = json.loads(response.data.decode())
        assert response.status_code == 200
        assert 'pong' in data['message']
        assert 'success' in data['status']
