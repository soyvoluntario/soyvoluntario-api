import json

from tests.base import BaseTestCase


class TestApi(BaseTestCase):
    """Test the api is working."""

    def test_ping(self):
        """Ping the app to ensure it's running."""
        response = self.client.get('/api/v1')
        data = json.loads(response.data.decode())
        assert response.status_code == 200
        assert data['status'] == 'success'
