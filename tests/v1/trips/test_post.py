import datetime

from tests.base import BaseTestCase
from tests.utils import add_municipality, add_town, add_user


class TripsTestCase(BaseTestCase):
    def test_add_trip(self):
        municipality = add_municipality('Aguascalientes')
        town = add_town('Aguascalientes', municipality.id)
        add_user('test', 'test@domain.com', 'test')
        token = self.get_token('test@domain.com', 'test')
        today = str(datetime.datetime.today().date())
        data = {
            'municipalityId': municipality.id,
            'destinationId': town.id,
            'size': 1,
            'date': today,
        }
        response, status_code = self.post_request('/api/v1/trips', data, token)
        assert status_code == 201
        assert response['status'] == 'success'
        assert response['message'] == 'Trip added'
        assert response['trip']['id'] == 1
        assert response['trip']['organizerId']
        assert response['trip']['municipalityId'] == municipality.id
        assert response['trip']['destinationId'] == town.id
        assert response['trip']['size'] == 1
        assert len(response['trip']['participants']) == 1
        assert response['trip']['date'] == today
        assert response['trip']['time'] is None
        assert response['trip']['active']
        assert not response['trip']['finished']
        assert 'information' in response['trip']

    def test_add_trip_all_data(self):
        municipality = add_municipality('Aguascalientes')
        town = add_town('Aguascalientes', municipality.id)
        add_user('test', 'test@domain.com', 'test')
        token = self.get_token('test@domain.com', 'test')
        today = str(datetime.datetime.today().date())
        data = {
            'municipalityId': municipality.id,
            'destinationId': town.id,
            'size': 1,
            'date': today,
            'time': '2:45 PM',
            'information': 'Some important info',
        }
        response, _ = self.post_request('/api/v1/trips', data, token)
        assert response['trip']['id'] == 1
        assert response['trip']['organizerId']
        assert response['trip']['municipalityId'] == municipality.id
        assert response['trip']['destinationId'] == town.id
        assert response['trip']['size'] == 1
        assert len(response['trip']['participants']) == 1
        assert response['trip']['date'] == today
        assert response['trip']['time'] == '14:45:00'
        assert response['trip']['active']
        assert not response['trip']['finished']
        assert response['trip']['information'] == 'Some important info'

    def test_add_trip_visitor(self):
        response, status_code = self.post_request('/api/v1/trips', {})
        assert status_code == 401
        assert response['status'] == 'error'
        assert response['message'] == 'Provide a valid token'

    def test_add_trip_no_data(self):
        add_user('test', 'test@domain.com', 'test')
        token = self.get_token('test@domain.com', 'test')
        response, status_code = self.post_request('/api/v1/trips', {}, token)
        assert status_code == 400
        assert response['status'] == 'fail'
        assert response['message'] == 'Empty request'

    def test_add_trip_no_municipality(self):
        town = add_town()
        add_user('test', 'test@domain.com', 'test')
        token = self.get_token('test@domain.com', 'test')
        today = str(datetime.datetime.today().date()),
        data = {
            'destinationId': town.id,
            'size': 1,
            'date': today,
        }
        response, status_code = self.post_request('/api/v1/trips', data, token)
        assert status_code == 400
        assert response['status'] == 'fail'
        assert response['message'] == 'Empty municipality'

    def test_add_trip_no_destination(self):
        municipality = add_municipality()
        add_user('test', 'test@domain.com', 'test')
        token = self.get_token('test@domain.com', 'test')
        today = str(datetime.datetime.today().date()),
        data = {
            'municipalityId': municipality.id,
            'size': 1,
            'date': today,
        }
        response, status_code = self.post_request('/api/v1/trips', data, token)
        assert status_code == 400
        assert response['status'] == 'fail'
        assert response['message'] == 'Empty destination'

    def test_add_trip_no_size(self):
        municipality = add_municipality()
        town = add_town()
        add_user('test', 'test@domain.com', 'test')
        token = self.get_token('test@domain.com', 'test')
        today = str(datetime.datetime.today().date()),
        data = {
            'municipalityId': municipality.id,
            'destinationId': town.id,
            'date': today,
        }
        response, status_code = self.post_request('/api/v1/trips', data, token)
        assert status_code == 400
        assert response['status'] == 'fail'
        assert response['message'] == 'Empty size'

    def test_add_trip_no_date(self):
        municipality = add_municipality()
        town = add_town()
        add_user('test', 'test@domain.com', 'test')
        token = self.get_token('test@domain.com', 'test')
        data = {
            'municipalityId': municipality.id,
            'destinationId': town.id,
            'size': 1,
        }
        response, status_code = self.post_request('/api/v1/trips', data, token)
        assert status_code == 400
        assert response['status'] == 'fail'
        assert response['message'] == 'Empty date'

    def test_add_trip_invalid_municipality(self):
        town = add_town()
        add_user('test', 'test@domain.com', 'test')
        token = self.get_token('test@domain.com', 'test')
        today = str(datetime.datetime.today().date()),
        data = {
            'municipalityId': 'one',
            'destinationId': town.id,
            'size': 1,
            'date': today,
        }
        response, status_code = self.post_request('/api/v1/trips', data, token)
        assert status_code == 422
        assert response['status'] == 'fail'
        assert response['message'] == 'Invalid municipality id'

    def test_add_trip_invalid_destination(self):
        municipality = add_municipality()
        add_user('test', 'test@domain.com', 'test')
        token = self.get_token('test@domain.com', 'test')
        today = str(datetime.datetime.today().date()),
        data = {
            'municipalityId': municipality.id,
            'destinationId': 'one',
            'size': 1,
            'date': today,
        }
        response, status_code = self.post_request('/api/v1/trips', data, token)
        assert status_code == 422
        assert response['status'] == 'fail'
        assert response['message'] == 'Invalid destination id'

    def test_add_trip_invalid_date(self):
        municipality = add_municipality()
        town = add_town()
        add_user('test', 'test@domain.com', 'test')
        token = self.get_token('test@domain.com', 'test')
        data = {
            'municipalityId': municipality.id,
            'destinationId': town.id,
            'size': 1,
            'date': 'tomorrow',
        }
        response, status_code = self.post_request('/api/v1/trips', data, token)
        assert status_code == 422
        assert response['status'] == 'fail'
        assert response['message'] == 'Invalid date format'

    def test_add_trip_invalid_size(self):
        municipality = add_municipality()
        town = add_town()
        add_user('test', 'test@domain.com', 'test')
        token = self.get_token('test@domain.com', 'test')
        today = str(datetime.datetime.today().date()),
        data = {
            'municipalityId': municipality.id,
            'destinationId': town.id,
            'size': 'one',
            'date': today,
        }
        response, status_code = self.post_request('/api/v1/trips', data, token)
        assert status_code == 422
        assert response['status'] == 'fail'
        assert response['message'] == 'Invalid size'

    def test_add_trip_invalid_time(self):
        municipality = add_municipality()
        town = add_town()
        add_user('test', 'test@domain.com', 'test')
        token = self.get_token('test@domain.com', 'test')
        today = str(datetime.datetime.today().date())
        data = {
            'municipalityId': municipality.id,
            'destinationId': town.id,
            'size': 1,
            'date': today,
            'time': 'morning',
        }
        response, status_code = self.post_request('/api/v1/trips', data, token)
        assert status_code == 422
        assert response['status'] == 'fail'
        assert response['message'] == 'Invalid time format'
