from datetime import datetime
from tests.base import BaseTestCase
from tests.utils import add_municipality, add_town, add_trip, add_user

from soyvoluntario.models import User


class TestTripsEndpoint(BaseTestCase):
    def test_trips(self):
        municipality = add_municipality('Aguascalientes')
        town = add_town('Aguascalientes', municipality.id)
        add_trip(municipality_id=municipality.id, destination_id=town.id)
        add_trip()
        user = add_user('test', 'test@domain.com', 'test')
        token = self.get_token('test@domain.com', 'test')
        today = str(datetime.today().date())
        time = '14:00:00'
        data = {
            'municipalityId': municipality.id,
            'destinationId': town.id,
            'size': 1,
            'date': today,
            'time': time,
            'information': 'Some important info',
        }
        self.post_request('/api/v1/trips', data, token)
        response, status_code = self.get_request('/api/v1/trips')
        assert status_code == 200
        assert response['status'] == 'success'
        assert len(response['trips']) == 3
        trip = response['trips'][0]
        assert trip['id'] == 3
        assert trip['organizerId'] == user.id
        assert trip['municipalityId'] == municipality.id
        assert trip['destinationId'] == town.id
        assert trip['size'] == 1
        assert trip['date'] == today
        assert trip['time'] == time
        assert trip['participantsNumber'] == 1
        assert trip['active']
        assert not trip['finished']
        assert 'information' in trip

    def test_trips_default_pagination(self):
        for _ in range(60):
            add_trip()
        response, status_code = self.get_request('/api/v1/trips')
        assert status_code == 200
        assert len(response['trips']) == 50
        assert response['page'] == 1
        assert response['pageSize'] == 50
        assert response['hasNext']

    def test_trips_all(self):
        for _ in range(60):
            add_trip()
        params = {'all': True}
        response, status_code = self.get_request('/api/v1/trips',
                                                 params=params)
        assert status_code == 200
        assert len(response['trips']) == 60

    def test_trips_pagination_hard_limit(self):
        for _ in range(60):
            add_trip()
        params = {'size': 60}
        response, status_code = self.get_request('/api/v1/trips',
                                                 params=params)
        assert status_code == 200
        assert len(response['trips']) == 50
        assert response['page'] == 1
        assert response['pageSize'] == 50
        assert response['hasNext']

    def test_trips_pagination(self):
        for _ in range(40):
            add_trip()
        params = {'size': 20, 'page': '1'}
        response1, _ = self.get_request('/api/v1/trips', params=params)
        params = {'size': 20, 'page': '2'}
        response2, _ = self.get_request('/api/v1/trips', params=params)
        assert response1['trips'] != response2['trips']
        assert response1['page'] == 1
        assert response1['pageSize'] == 20
        assert response1['hasNext']
        assert response2['page'] == 2
        assert response2['pageSize'] == 20
        assert not response2['hasNext']

    def test_trips_large_page_number(self):
        for _ in range(40):
            add_trip()
        params = {'size': 20, 'page': '5'}
        response, _ = self.get_request('/api/v1/trips', params=params)
        assert len(response['trips']) == 20
        assert response['page'] == 2
        assert response['pageSize'] == 20
        assert not response['hasNext']

    def test_trips_negative_page_number(self):
        for _ in range(40):
            add_trip()
        params = {'size': 20, 'page': '-1'}
        response, _ = self.get_request('/api/v1/trips', params=params)
        assert len(response['trips']) == 20
        assert response['page'] == 1
        assert response['pageSize'] == 20
        assert response['hasNext']

    def test_trips_invalid_page_number(self):
        for _ in range(40):
            add_trip()
        params = {'size': 20, 'page': 'first'}
        response, status_code = self.get_request('/api/v1/trips',
                                                 params=params)
        assert status_code == 422
        assert response['status'] == 'fail'
        assert response['message'] == 'Invalid page number'

    def test_trips_invalid_page_size(self):
        for _ in range(40):
            add_trip()
        params = {'size': 'large', 'page': 1}
        response, status_code = self.get_request('/api/v1/trips',
                                                 params=params)
        assert status_code == 422
        assert response['status'] == 'fail'
        assert response['message'] == 'Invalid page size'

    def test_get_trip(self):
        trip = add_trip()
        organizer = User.query.filter_by(id=trip.organizer.id).first()
        participant = add_user('test', 'test@domain.com', 'test')
        trip.participants.append(participant)
        endpoint = '/api/v1/trips/%s' % trip.id
        response, status_code = self.get_request(endpoint)
        assert status_code == 200
        assert response['status'] == 'success'
        assert response['trip']['id'] == trip.id
        assert response['trip']['organizerId'] == organizer.id
        assert response['trip']['municipalityId'] == trip.municipality.id
        assert response['trip']['destinationId'] == trip.destination.id
        assert response['trip']['size'] == 3
        assert 'date' in response['trip']
        assert 'time' in response['trip']
        assert response['trip']['active']
        assert not response['trip']['finished']
        assert 'information' in response['trip']
        participants = response['trip']['participants']
        assert len(participants) == 2
        assert participants[0]['id'] == trip.organizer.id
        assert participants[1]['id'] == participant.id

    def test_get_trip_non_existing(self):
        trip = add_trip()
        endpoint = '/api/v1/trips/%s' % (trip.id + 3)
        response, status_code = self.get_request(endpoint)
        assert status_code == 404
        assert response['status'] == 'fail'
        assert response['message'] == 'Trip not found'

    def test_get_trip_invalid_id(self):
        add_trip()
        endpoint = '/api/v1/trips/%s' % 'trip'
        response, status_code = self.get_request(endpoint)
        assert status_code == 422
        assert response['status'] == 'fail'
        assert response['message'] == 'Invalid id'

    def test_get_trip_participants(self):
        trip = add_trip()
        participant = add_user('test', 'test@domain.com', 'test')
        trip.participants.append(participant)
        token = self.get_token('test@domain.com', 'test')
        endpoint = '/api/v1/trips/%s/participants' % trip.id
        response, status_code = self.get_request(endpoint, token)
        assert status_code == 200
        assert response['status'] == 'success'
        participants = response['participants']
        assert len(participants) == 2
        assert participants[0]['id'] == trip.organizer.id
        assert 'name' in participants[0]
        assert 'email' not in participants[0]
        assert 'cellphone' in participants[0]
        assert 'twitterHandle' in participants[0]

    def test_get_trip_participants_visitor(self):
        trip = add_trip()
        participant = add_user('test', 'test@domain.com', 'test')
        trip.participants.append(participant)
        endpoint = '/api/v1/trips/%s/participants' % trip.id
        response, status_code = self.get_request(endpoint)
        assert status_code == 200
        assert response['status'] == 'success'
        participants = response['participants']
        assert len(participants) == 2
        assert participants[0]['id'] == trip.organizer.id
        assert 'name' in participants[0]
        assert 'email' not in participants[0]
        assert 'cellphone' not in participants[0]
        assert 'twitterHandle' not in participants[0]
