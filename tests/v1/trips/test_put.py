from tests.base import BaseTestCase
from tests.utils import add_municipality, add_town, add_trip, add_user


class TestTripsEndpoint(BaseTestCase):
    def test_join_trip(self):
        trip = add_trip()
        user = add_user('test', 'test@domain.com', 'test')
        token = self.get_token('test@domain.com', 'test')
        endpoint = '/api/v1/trips/%s/join' % trip.id
        response, status_code = self.put_request(endpoint, {}, token)
        assert status_code == 200
        assert response['status'] == 'success'
        assert response['message'] == 'Joined the trip'
        assert user in trip.participants

    def test_join_trip_visitor(self):
        trip = add_trip()
        endpoint = '/api/v1/trips/%s/join' % trip.id
        response, status_code = self.put_request(endpoint, {})
        assert status_code == 401
        assert response['status'] == 'error'
        assert response['message'] == 'Provide a valid token'

    def test_join_full_trip(self):
        trip = add_trip(size=1)
        organizer = add_user('organizer', 'organizer@domain.com', 'test')
        trip.participants.append(organizer)
        add_user('test', 'test@domain.com', 'test')
        token = self.get_token('test@domain.com', 'test')
        endpoint = '/api/v1/trips/%s/join' % trip.id
        response, status_code = self.put_request(endpoint, {}, token)
        assert status_code == 409
        assert response['status'] == 'fail'
        assert response['message'] == 'The trip is full'

    def test_join_trip_already_joined(self):
        trip = add_trip(size=2)
        add_user('test', 'test@domain.com', 'test')
        token = self.get_token('test@domain.com', 'test')
        endpoint = '/api/v1/trips/%s/join' % trip.id
        response, status_code = self.put_request(endpoint, {}, token)
        response, status_code = self.put_request(endpoint, {}, token)
        assert status_code == 200
        assert response['status'] == 'success'
        assert response['message'] == 'Joined the trip'

    def test_leave_trip(self):
        trip = add_trip()
        user = add_user('test', 'test@domain.com', 'test')
        token = self.get_token('test@domain.com', 'test')
        endpoint = '/api/v1/trips/%s/join' % trip.id
        self.put_request(endpoint, {}, token)
        endpoint = '/api/v1/trips/%s/leave' % trip.id
        response, status_code = self.put_request(endpoint, {}, token)
        assert status_code == 200
        assert response['status'] == 'success'
        assert response['message'] == 'Left the trip'
        assert user not in trip.participants

    def test_leave_not_in_trip(self):
        trip = add_trip()
        user = add_user('test', 'test@domain.com', 'test')
        token = self.get_token('test@domain.com', 'test')
        endpoint = '/api/v1/trips/%s/leave' % trip.id
        response, status_code = self.put_request(endpoint, {}, token)
        assert status_code == 200
        assert response['status'] == 'success'
        assert response['message'] == 'Left the trip'
        assert user not in trip.participants

    def test_modify_trip(self):
        organizer = add_user('test', 'test@domain.com', 'test')
        trip = add_trip(organizer_id=organizer.id)
        token = self.get_token('test@domain.com', 'test')
        endpoint = '/api/v1/trips/%s' % trip.id
        response, status_code = self.put_request(endpoint, {}, token)
        assert status_code == 200
        assert response['message'] == 'Trip modified'
        assert response['status'] == 'success'
        assert response['trip']['id'] == trip.id
        assert response['trip']['organizerId'] == organizer.id
        assert response['trip']['municipalityId'] == trip.municipality.id
        assert response['trip']['destinationId'] == trip.destination.id
        assert response['trip']['size'] == trip.size
        assert response['trip']['date'] == trip.date.isoformat()
        assert response['trip']['time'] is None
        assert response['trip']['active']
        assert not response['trip']['finished']
        assert 'information' in response['trip']
        participants = response['trip']['participants']
        assert len(participants) == 1
        assert participants[0]['id'] == trip.organizer.id

    def test_modify_trip_invalid_id(self):
        organizer = add_user('test', 'test@domain.com', 'test')
        trip = add_trip(organizer_id=organizer.id)
        token = self.get_token('test@domain.com', 'test')
        endpoint = '/api/v1/trips/%s' % (trip.id + 3)
        data = {}
        response, status_code = self.put_request(endpoint, data, token)
        assert status_code == 404
        assert response['status'] == 'fail'
        assert response['message'] == 'Trip not found'

    def test_modify_trip_invalid_id_format(self):
        add_user('test', 'test@domain.com', 'test')
        token = self.get_token('test@domain.com', 'test')
        endpoint = '/api/v1/trips/%s' % 'trip'
        data = {}
        response, status_code = self.put_request(endpoint, data, token)
        assert status_code == 404
        assert response['status'] == 'fail'
        assert response['message'] == 'Trip not found'

    def test_modify_trip_municipality(self):
        municipality = add_municipality('Aguascalientes')
        organizer = add_user('test', 'test@domain.com', 'test')
        trip = add_trip(organizer_id=organizer.id)
        token = self.get_token('test@domain.com', 'test')
        endpoint = '/api/v1/trips/%s' % trip.id
        data = {
            'municipality_id': municipality.id,
        }
        response, _ = self.put_request(endpoint, data, token)
        assert response['trip']['municipalityId'] == municipality.id

    def test_modify_trip_invalid_municipality(self):
        organizer = add_user('test', 'test@domain.com', 'test')
        trip = add_trip(organizer_id=organizer.id)
        token = self.get_token('test@domain.com', 'test')
        endpoint = '/api/v1/trips/%s' % trip.id
        data = {
            'municipality_id': 'one',
        }
        response, status_code = self.put_request(endpoint, data, token)
        assert status_code == 422
        assert response['status'] == 'fail'
        assert response['message'] == 'Invalid municipality id'

    def test_modify_trip_destination(self):
        destination = add_town('Aguascalientes')
        organizer = add_user('test', 'test@domain.com', 'test')
        trip = add_trip(organizer_id=organizer.id)
        token = self.get_token('test@domain.com', 'test')
        endpoint = '/api/v1/trips/%s' % trip.id
        data = {
            'destination_id': destination.id,
        }
        response, _ = self.put_request(endpoint, data, token)
        assert response['trip']['destinationId'] == destination.id

    def test_modify_trip_invalid_destination(self):
        organizer = add_user('test', 'test@domain.com', 'test')
        trip = add_trip(organizer_id=organizer.id)
        token = self.get_token('test@domain.com', 'test')
        endpoint = '/api/v1/trips/%s' % trip.id
        data = {
            'destination_id': 'one',
        }
        response, status_code = self.put_request(endpoint, data, token)
        assert status_code == 422
        assert response['status'] == 'fail'
        assert response['message'] == 'Invalid destination id'

    def test_modify_trip_date(self):
        organizer = add_user('test', 'test@domain.com', 'test')
        trip = add_trip(organizer_id=organizer.id)
        token = self.get_token('test@domain.com', 'test')
        endpoint = '/api/v1/trips/%s' % trip.id
        data = {
            'date': '2010/10/1',
        }
        response, _ = self.put_request(endpoint, data, token)
        assert response['trip']['date'] == '2010-10-01'

    def test_modify_trip_invalid_date(self):
        organizer = add_user('test', 'test@domain.com', 'test')
        trip = add_trip(organizer_id=organizer.id)
        token = self.get_token('test@domain.com', 'test')
        endpoint = '/api/v1/trips/%s' % trip.id
        data = {
            'date': 'tomorrow',
        }
        response, status_code = self.put_request(endpoint, data, token)
        assert status_code == 422
        assert response['status'] == 'fail'
        assert response['message'] == 'Invalid date format'

    def test_modify_trip_time(self):
        organizer = add_user('test', 'test@domain.com', 'test')
        trip = add_trip(organizer_id=organizer.id)
        token = self.get_token('test@domain.com', 'test')
        endpoint = '/api/v1/trips/%s' % trip.id
        data = {
            'time': '1:30 PM',
        }
        response, status_code = self.put_request(endpoint, data, token)
        assert response['trip']['time'] == '13:30:00'

    def test_modify_trip_invalid_time(self):
        organizer = add_user('test', 'test@domain.com', 'test')
        trip = add_trip(organizer_id=organizer.id)
        token = self.get_token('test@domain.com', 'test')
        endpoint = '/api/v1/trips/%s' % trip.id
        data = {
            'time': 'morning',
        }
        response, status_code = self.put_request(endpoint, data, token)
        assert status_code == 422
        assert response['status'] == 'fail'
        assert response['message'] == 'Invalid time format'

    def test_modify_trip_invalid_size(self):
        organizer = add_user('test', 'test@domain.com', 'test')
        trip = add_trip(organizer_id=organizer.id)
        token = self.get_token('test@domain.com', 'test')
        endpoint = '/api/v1/trips/%s' % trip.id
        data = {
            'size': 0,
        }
        response, status_code = self.put_request(endpoint, data, token)
        assert status_code == 409
        assert response['status'] == 'fail'
        assert response['message'] == 'New size smaller than participants'

    def test_modify_trip_size(self):
        organizer = add_user('test', 'test@domain.com', 'test')
        trip = add_trip(organizer_id=organizer.id)
        token = self.get_token('test@domain.com', 'test')
        endpoint = '/api/v1/trips/%s' % trip.id
        data = {
            'size': 10,
        }
        response, status_code = self.put_request(endpoint, data, token)
        assert response['trip']['size'] == 10

    def test_modify_trip_invalid_size_format(self):
        organizer = add_user('test', 'test@domain.com', 'test')
        trip = add_trip(organizer_id=organizer.id)
        token = self.get_token('test@domain.com', 'test')
        endpoint = '/api/v1/trips/%s' % trip.id
        data = {
            'size': 'one',
        }
        response, status_code = self.put_request(endpoint, data, token)
        assert status_code == 422
        assert response['status'] == 'fail'
        assert response['message'] == 'Invalid size'

    def test_modify_trip_information(self):
        organizer = add_user('test', 'test@domain.com', 'test')
        trip = add_trip(organizer_id=organizer.id)
        token = self.get_token('test@domain.com', 'test')
        endpoint = '/api/v1/trips/%s' % trip.id
        data = {
            'information': 'This is the description',
        }
        response, _ = self.put_request(endpoint, data, token)
        assert response['trip']['information'] == 'This is the description'

    def test_cancel_trip(self):
        organizer = add_user('test', 'test@domain.com', 'test')
        trip = add_trip(organizer_id=organizer.id)
        token = self.get_token('test@domain.com', 'test')
        endpoint = '/api/v1/trips/%s' % trip.id
        data = {
            'active': False,
        }
        response, _ = self.put_request(endpoint, data, token)
        assert not response['trip']['active']

    def test_finish_trip(self):
        organizer = add_user('test', 'test@domain.com', 'test')
        trip = add_trip(organizer_id=organizer.id)
        token = self.get_token('test@domain.com', 'test')
        endpoint = '/api/v1/trips/%s' % trip.id
        data = {
            'finished': True,
        }
        response, _ = self.put_request(endpoint, data, token)
        assert response['trip']['finished']

    def test_modify_finished_trip(self):
        organizer = add_user('test', 'test@domain.com', 'test')
        trip = add_trip(organizer_id=organizer.id)
        token = self.get_token('test@domain.com', 'test')
        endpoint = '/api/v1/trips/%s' % trip.id
        data = {
            'size': 10,
        }
        trip.finished = True
        response, status_code = self.put_request(endpoint, data, token)
        assert status_code == 409
        assert response['status'] == 'fail'
        assert response['message'] == 'Finished trips should not change'

    def test_modify_trip_unauthorized_user(self):
        trip = add_trip()
        add_user('test', 'test@domain.com', 'test')
        token = self.get_token('test@domain.com', 'test')
        endpoint = '/api/v1/trips/%s' % trip.id
        data = {
            'size': 0,
        }
        response, status_code = self.put_request(endpoint, data, token)
        assert status_code == 403
        assert response['status'] == 'error'
        assert response['message'] == 'Unauthorized user'
