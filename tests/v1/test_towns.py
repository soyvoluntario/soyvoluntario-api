from tests.base import BaseTestCase
from tests.utils import add_municipality, add_town, add_trip


class TestTownsEndpoint(BaseTestCase):
    def test_towns(self):
        municipality = add_municipality('Aguascalientes')
        add_town('Aguascalientes', municipality.id)
        town = add_town('Asientos', municipality.id)
        add_trip(None, municipality.id, town.id)
        response, status_code = self.get_request('/api/v1/towns')
        assert status_code == 200
        assert response['status'] == 'success'
        assert len(response['towns']) == 2
        assert response['towns'][0]['name'] == 'Asientos'
        assert response['towns'][1]['name'] == 'Aguascalientes'
        assert response['towns'][0]['hasTrips']
        assert not response['towns'][1]['hasTrips']
        assert 'id' in response['towns'][0]
        assert 'clge' in response['towns'][0]
        assert 'longitude' in response['towns'][0]
        assert 'latitude' in response['towns'][0]
        assert 'municipalityId' in response['towns'][0]

    def test_towns_default_pagination(self):
        for _ in range(60):
            add_town()
        response, status_code = self.get_request('/api/v1/towns')
        assert status_code == 200
        assert len(response['towns']) == 50
        assert response['page'] == 1
        assert response['pageSize'] == 50
        assert response['hasNext']

    def test_towns_all(self):
        for _ in range(60):
            add_town()
        params = {'all': True}
        response, status_code = self.get_request('/api/v1/towns',
                                                 params=params)
        assert status_code == 200
        assert len(response['towns']) == 60

    def test_towns_pagination_hard_limit(self):
        for _ in range(60):
            add_town()
        params = {'size': 60}
        response, status_code = self.get_request('/api/v1/towns',
                                                 params=params)
        assert status_code == 200
        assert len(response['towns']) == 50
        assert response['page'] == 1
        assert response['pageSize'] == 50
        assert response['hasNext']

    def test_towns_pagination(self):
        for _ in range(40):
            add_town()
        params = {'size': 20, 'page': '1'}
        response1, _ = self.get_request('/api/v1/towns', params=params)
        params = {'size': 20, 'page': '2'}
        response2, _ = self.get_request('/api/v1/towns', params=params)
        assert response1['towns'] != response2['towns']
        assert response1['page'] == 1
        assert response1['pageSize'] == 20
        assert response1['hasNext']
        assert response2['page'] == 2
        assert response2['pageSize'] == 20
        assert not response2['hasNext']

    def test_towns_large_page_number(self):
        for _ in range(40):
            add_town()
        params = {'size': 20, 'page': '5'}
        response, _ = self.get_request('/api/v1/towns', params=params)
        assert len(response['towns']) == 20
        assert response['page'] == 2
        assert response['pageSize'] == 20
        assert not response['hasNext']

    def test_towns_negative_page_number(self):
        for _ in range(40):
            add_town()
        params = {'size': 20, 'page': '-1'}
        response, _ = self.get_request('/api/v1/towns', params=params)
        assert len(response['towns']) == 20
        assert response['page'] == 1
        assert response['pageSize'] == 20
        assert response['hasNext']

    def test_towns_invalid_page_number(self):
        for _ in range(40):
            add_town()
        params = {'size': 20, 'page': 'first'}
        response, status_code = self.get_request('/api/v1/towns',
                                                 params=params)
        assert status_code == 422
        assert response['status'] == 'fail'
        assert response['message'] == 'Invalid page number'

    def test_towns_invalid_page_size(self):
        for _ in range(40):
            add_town()
        params = {'size': 'large', 'page': 1}
        response, status_code = self.get_request('/api/v1/towns',
                                                 params=params)
        assert status_code == 422
        assert response['status'] == 'fail'
        assert response['message'] == 'Invalid page size'

    def test_get_town(self):
        add_trip()
        response, status_code = self.get_request('/api/v1/towns/1')
        assert status_code == 200
        assert 'id' in response['town']
        assert 'name' in response['town']
        assert 'longitude' in response['town']
        assert 'latitude' in response['town']
        assert len(response['town']['trips']) == 1
        assert 'id' in response['town']['trips'][0]

    def test_get_non_existing(self):
        town = add_town()
        endpoint = '/api/v1/towns/%s' % (town.id + 3)
        response, status_code = self.get_request(endpoint)
        assert status_code == 404
        assert response['status'] == 'fail'
        assert response['message'] == 'Town not found'

    def test_get_invalid_id(self):
        add_town()
        response, status_code = self.get_request('/api/v1/towns/one')
        assert status_code == 422
        assert response['status'] == 'fail'
        assert response['message'] == 'Invalid id'

    def test_get_towns_trips(self):
        town = add_town()
        add_trip(destination_id=town.id)
        response, status_code = self.get_request('/api/v1/towns/1/trips')
        assert status_code == 200
        assert 'id' in response['trips'][0]
        assert 'active' in response['trips'][0]
        assert 'finished' in response['trips'][0]
        assert 'date' in response['trips'][0]
        assert 'time' in response['trips'][0]
        assert 'municipalityId' in response['trips'][0]
        assert 'organizerId' in response['trips'][0]
        assert 'size' in response['trips'][0]
        assert 'information' in response['trips'][0]
        assert 'participantsNumber' in response['trips'][0]
