from tests.base import BaseTestCase
from tests.utils import add_municipality, add_state


class TestTownsEndpoint(BaseTestCase):
    def test_states(self):
        add_state('Aguascalientes')
        response, status_code = self.get_request('/api/v1/states')
        assert status_code == 200
        assert response['status'] == 'success'
        assert len(response['states']) == 1
        assert response['states'][0]['id'] == 1
        assert response['states'][0]['name'] == 'Aguascalientes'
        assert 'cagee' in response['states'][0]

    def test_states_default_pagination(self):
        for _ in range(60):
            add_state()
        response, status_code = self.get_request('/api/v1/states')
        assert status_code == 200
        assert len(response['states']) == 50
        assert response['page'] == 1
        assert response['pageSize'] == 50
        assert response['hasNext']

    def test_states_all(self):
        for _ in range(60):
            add_state()
        params = {'all': True}
        response, status_code = self.get_request('/api/v1/states',
                                                 params=params)
        assert status_code == 200
        assert len(response['states']) == 60

    def test_states_pagination_hard_limit(self):
        for _ in range(60):
            add_state()
        params = {'size': 60}
        response, status_code = self.get_request('/api/v1/states',
                                                 params=params)
        assert status_code == 200
        assert len(response['states']) == 50
        assert response['page'] == 1
        assert response['pageSize'] == 50
        assert response['hasNext']

    def test_states_pagination(self):
        for _ in range(40):
            add_state()
        params = {'size': 20, 'page': '1'}
        response1, _ = self.get_request('/api/v1/states', params=params)
        params = {'size': 20, 'page': '2'}
        response2, _ = self.get_request('/api/v1/states', params=params)
        assert response1['states'] != response2['states']
        assert response1['page'] == 1
        assert response1['pageSize'] == 20
        assert response1['hasNext']
        assert response2['page'] == 2
        assert response2['pageSize'] == 20
        assert not response2['hasNext']

    def test_states_large_page_number(self):
        for _ in range(40):
            add_state()
        params = {'size': 20, 'page': '5'}
        response, _ = self.get_request('/api/v1/states', params=params)
        assert len(response['states']) == 20
        assert response['page'] == 2
        assert response['pageSize'] == 20
        assert not response['hasNext']

    def test_states_negative_page_number(self):
        for _ in range(40):
            add_state()
        params = {'size': 20, 'page': '-1'}
        response, _ = self.get_request('/api/v1/states', params=params)
        assert len(response['states']) == 20
        assert response['page'] == 1
        assert response['pageSize'] == 20
        assert response['hasNext']

    def test_states_invalid_page_number(self):
        for _ in range(40):
            add_state()
        params = {'size': 20, 'page': 'first'}
        response, status_code = self.get_request('/api/v1/states',
                                                 params=params)
        assert status_code == 422
        assert response['status'] == 'fail'
        assert response['message'] == 'Invalid page number'

    def test_states_invalid_page_size(self):
        for _ in range(40):
            add_state()
        params = {'size': 'large', 'page': 1}
        response, status_code = self.get_request('/api/v1/states',
                                                 params=params)
        assert status_code == 422
        assert response['status'] == 'fail'
        assert response['message'] == 'Invalid page size'

    def test_get_state(self):
        add_municipality()
        response, status_code = self.get_request('/api/v1/states/1')
        assert status_code == 200
        assert 'id' in response['state']
        assert 'name' in response['state']
        assert 'cagee' in response['state']
        assert len(response['state']['municipalities']) == 1
        assert 'id' in response['state']['municipalities'][0]

    def test_get_non_existing(self):
        state = add_state()
        endpoint = '/api/v1/states/%s' % (state.id + 3)
        response, status_code = self.get_request(endpoint)
        assert status_code == 404
        assert response['status'] == 'fail'
        assert response['message'] == 'State not found'

    def test_get_invalid_id(self):
        add_municipality()
        response, status_code = self.get_request('/api/v1/states/one')
        assert status_code == 422
        assert response['status'] == 'fail'
        assert response['message'] == 'Invalid id'

    def test_get_state_municipalities(self):
        add_municipality()
        endpoint = '/api/v1/states/1/municipalities'
        response, status_code = self.get_request(endpoint)
        assert status_code == 200
        assert 'id' in response['municipalities'][0]
        assert 'name' in response['municipalities'][0]
        assert 'cagem' in response['municipalities'][0]
