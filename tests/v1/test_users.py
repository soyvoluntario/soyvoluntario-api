from tests.base import BaseTestCase
from tests.utils import add_user, add_trip


class TestUsersEndpoint(BaseTestCase):
    def test_no_plaintext_password(self):
        user = add_user('test', 'test@domain.com', 'test')
        assert user.password != 'test'

    def test_users(self):
        add_user('Test User', 'test@domain.com', 'test')
        response, status_code = self.get_request('/api/v1/users')
        assert status_code == 200
        assert response['status'] == 'success'
        assert len(response['users']) == 1
        assert response['users'][0]['id'] == 1
        assert response['users'][0]['name'] == 'Test User'
        assert 'cellphone' not in response['users'][0]
        assert 'twitterHandle' not in response['users'][0]

    def test_users_default_pagination(self):
        for number in range(60):
            add_user('Test User', number, 'test')
        response, status_code = self.get_request('/api/v1/users')
        assert status_code == 200
        assert len(response['users']) == 50
        assert response['page'] == 1
        assert response['pageSize'] == 50
        assert response['hasNext']

    def test_users_all(self):
        for number in range(60):
            add_user('Test User', number, 'test')
        params = {'all': True}
        response, status_code = self.get_request('/api/v1/users',
                                                 params=params)
        assert status_code == 200
        assert len(response['users']) == 60

    def test_users_pagination_hard_limit(self):
        for number in range(60):
            add_user('Test User', number, 'test')
        params = {'size': 60}
        response, status_code = self.get_request('/api/v1/users',
                                                 params=params)
        assert status_code == 200
        assert len(response['users']) == 50
        assert response['page'] == 1
        assert response['pageSize'] == 50
        assert response['hasNext']

    def test_users_pagination(self):
        for number in range(40):
            add_user('Test User', number, 'test')
        params = {'size': 20, 'page': '1'}
        response1, _ = self.get_request('/api/v1/users', params=params)
        params = {'size': 20, 'page': '2'}
        response2, _ = self.get_request('/api/v1/users', params=params)
        assert response1['users'] != response2['users']
        assert response1['page'] == 1
        assert response1['pageSize'] == 20
        assert response1['hasNext']
        assert not response2['hasNext']

    def test_users_large_page_number(self):
        for number in range(40):
            add_user('Test User', number, 'test')
        params = {'size': 20, 'page': '5'}
        response, _ = self.get_request('/api/v1/users', params=params)
        assert response['users']
        assert response['page'] == 2
        assert response['pageSize'] == 20
        assert not response['hasNext']

    def test_users_negative_page_number(self):
        for number in range(40):
            add_user('Test User', number, 'test')
        params = {'size': 20, 'page': '-1'}
        response, _ = self.get_request('/api/v1/users', params=params)
        assert len(response['users']) == 20
        assert response['page'] == 1
        assert response['pageSize'] == 20
        assert response['hasNext']

    def test_states_invalid_page_number(self):
        for number in range(40):
            add_user('Test User', number, 'test')
        params = {'size': 20, 'page': 'first'}
        response, status_code = self.get_request('/api/v1/users',
                                                 params=params)
        assert status_code == 422
        assert response['status'] == 'fail'
        assert response['message'] == 'Invalid page number'

    def test_states_invalid_page_size(self):
        for number in range(40):
            add_user('Test User', number, 'test')
        params = {'size': 'large', 'page': 1}
        response, status_code = self.get_request('/api/v1/users',
                                                 params=params)
        assert status_code == 422
        assert response['status'] == 'fail'
        assert response['message'] == 'Invalid page size'

    def test_get_user(self):
        add_user('Test User', 'test@domain.com', 'test')
        response, status_code = self.get_request('/api/v1/users/1')
        assert status_code == 200
        assert response['status'] == 'success'
        assert response['user']['id'] == 1
        assert response['user']['name'] == 'Test User'
        assert 'cellphone' not in response['user']
        assert 'twitterHandle' not in response['user']
        assert 'organizedTrips' in response['user']
        assert 'trips' in response['user']

    def test_get_non_existing(self):
        response, status_code = self.get_request('/api/v1/users/1')
        assert status_code == 404
        assert response['status'] == 'fail'
        assert response['message'] == 'User not found'

    def test_get_invalid_id(self):
        response, status_code = self.get_request('/api/v1/states/one')
        assert status_code == 422
        assert response['status'] == 'fail'
        assert response['message'] == 'Invalid id'

    def test_get_user_organized_trips(self):
        add_user('Test User', 'test@domain.com', 'test')
        add_trip(organizer_id=1)
        endpoint = '/api/v1/users/1/organizedtrips'
        response, status_code = self.get_request(endpoint)
        assert status_code == 200
        assert response['status'] == 'success'
        assert 'id' in response['organizedTrips'][0]
        assert 'active' in response['organizedTrips'][0]
        assert 'finished' in response['organizedTrips'][0]
        assert 'date' in response['organizedTrips'][0]
        assert 'time' in response['organizedTrips'][0]
        assert 'destinationId' in response['organizedTrips'][0]
        assert 'municipalityId' in response['organizedTrips'][0]
        assert 'size' in response['organizedTrips'][0]
        assert 'information' in response['organizedTrips'][0]
        assert 'participantsNumber' in response['organizedTrips'][0]

    def test_get_user_trips(self):
        add_user('Test User', 'test@domain.com', 'test')
        add_trip(organizer_id=1)
        response, status_code = self.get_request('/api/v1/users/1/trips')
        assert status_code == 200
        assert response['status'] == 'success'
        assert 'id' in response['trips'][0]
        assert 'active' in response['trips'][0]
        assert 'finished' in response['trips'][0]
        assert 'date' in response['trips'][0]
        assert 'time' in response['trips'][0]
        assert 'destinationId' in response['trips'][0]
        assert 'municipalityId' in response['trips'][0]
        assert 'organizerId' in response['trips'][0]
        assert 'size' in response['trips'][0]
        assert 'information' in response['trips'][0]
        assert 'participantsNumber' in response['trips'][0]

    def test_add_user(self):
        user_data = {
            'name': 'Test User',
            'email': 'test@domain.com',
            'password': 'test',
        }
        response, status_code = self.post_request('/api/v1/users', user_data)
        assert status_code == 201
        assert response['status'] == 'success'
        assert response['message'] == 'Test User was added'
        assert response['profile']['id'] == 1
        assert response['profile']['name'] == 'Test User'
        assert 'cellphone' in response['profile']
        assert 'twitterHandle' in response['profile']

    def test_add_user_all_data(self):
        user_data = {
            'name': 'Test User',
            'email': 'test@domain.com',
            'password': 'test',
            'twitterHandle': 'test',
            'cellphone': "123456789",
        }
        response, status_code = self.post_request('/api/v1/users', user_data)
        assert status_code == 201
        assert response['status'] == 'success'
        assert response['message'] == 'Test User was added'
        assert response['profile']['id'] == 1
        assert response['profile']['name'] == 'Test User'
        assert response['profile']['twitterHandle'] == 'test'
        assert response['profile']['cellphone'] == '123456789'

    def test_add_user_no_data(self):
        response, status_code = self.post_request('/api/v1/users', {})
        assert status_code == 400
        assert response['status'] == 'fail'
        assert response['message'] == 'Empty request'

    def test_add_user_no_name(self):
        user_data = {
            'email': 'test@domain.com',
            'password': 'test',
        }
        response, status_code = self.post_request('/api/v1/users', user_data)
        assert status_code == 400
        assert response['status'] == 'fail'
        assert response['message'] == 'Empty name'

    def test_add_user_no_email(self):
        user_data = {
            'name': 'Test User',
            'password': 'test',
        }
        response, status_code = self.post_request('/api/v1/users', user_data)
        assert status_code == 400
        assert response['status'] == 'fail'
        assert response['message'] == 'Empty email'

    def test_add_user_no_password(self):
        user_data = {
            'name': 'Test User',
            'email': 'test@domain.com',
        }
        response, status_code = self.post_request('/api/v1/users', user_data)
        assert status_code == 400
        assert response['status'] == 'fail'
        assert response['message'] == 'Empty password'

    def test_add_user_invalid_email(self):
        user_data = {
            'name': 'Test User',
            'email': 'test-is-not-an-email',
            'password': 'test',
        }
        response, status_code = self.post_request('/api/v1/users', user_data)
        assert status_code == 422
        assert response['status'] == 'fail'
        assert response['message'] == 'Invalid email'

    def test_add_existing_user(self):
        add_user('test', 'test@domain.com', 'test')
        user_data = {
            'name': 'Test User',
            'email': 'test@domain.com',
            'password': 'test',
        }
        response, status_code = self.post_request('/api/v1/users', user_data)
        assert status_code == 409
        assert response['status'] == 'fail'
        assert response['message'] == 'Existing user'
