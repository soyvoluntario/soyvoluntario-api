from tests.base import BaseTestCase
from tests.utils import add_municipality, add_town, add_trip


class TestTownsEndpoint(BaseTestCase):
    def test_municipalities(self):
        add_municipality('Aguascalientes')
        response, status_code = self.get_request('/api/v1/municipalities')
        assert status_code == 200
        assert response['status'] == 'success'
        assert len(response['municipalities']) == 1
        assert response['municipalities'][0]['id'] == 1
        assert response['municipalities'][0]['name'] == 'Aguascalientes'
        assert 'stateId' in response['municipalities'][0]
        assert 'cagem' in response['municipalities'][0]

    def test_municipalities_default_pagination(self):
        for _ in range(60):
            add_municipality()
        response, status_code = self.get_request('/api/v1/municipalities')
        assert status_code == 200
        assert len(response['municipalities']) == 50
        assert response['page'] == 1
        assert response['pageSize'] == 50
        assert response['hasNext']

    def test_municipalities_all(self):
        for _ in range(60):
            add_municipality()
        params = {'all': True}
        response, status_code = self.get_request('/api/v1/municipalities',
                                                 params=params)
        assert status_code == 200
        assert len(response['municipalities']) == 60

    def test_municipalities_pagination_hard_limit(self):
        for _ in range(60):
            add_municipality()
        params = {'size': 60}
        response, status_code = self.get_request('/api/v1/municipalities',
                                                 params=params)
        assert status_code == 200
        assert len(response['municipalities']) == 50
        assert response['page'] == 1
        assert response['pageSize'] == 50
        assert response['hasNext']

    def test_municipalities_pagination(self):
        for _ in range(40):
            add_municipality()
        params = {'size': 20, 'page': '1'}
        response1, _ = self.get_request('/api/v1/municipalities',
                                        params=params)
        params = {'size': 20, 'page': '2'}
        response2, _ = self.get_request('/api/v1/municipalities',
                                        params=params)
        assert response1['municipalities'] != response2['municipalities']
        assert response1['page'] == 1
        assert response1['pageSize'] == 20
        assert response1['hasNext']
        assert response2['page'] == 2
        assert response2['pageSize'] == 20
        assert not response2['hasNext']

    def test_municipalities_large_page_number(self):
        for _ in range(40):
            add_municipality()
        params = {'size': 20, 'page': '5'}
        response, _ = self.get_request('/api/v1/municipalities', params=params)
        assert len(response['municipalities']) == 20
        assert response['page'] == 2
        assert response['pageSize'] == 20
        assert not response['hasNext']

    def test_municipalities_negative_page_number(self):
        for _ in range(40):
            add_municipality()
        params = {'size': 20, 'page': '-1'}
        response, _ = self.get_request('/api/v1/municipalities', params=params)
        assert len(response['municipalities']) == 20
        assert response['page'] == 1
        assert response['pageSize'] == 20
        assert response['hasNext']

    def test_municipalities_invalid_page_number(self):
        for _ in range(40):
            add_municipality()
        params = {'size': 20, 'page': 'first'}
        response, status_code = self.get_request('/api/v1/municipalities',
                                                 params=params)
        assert status_code == 422
        assert response['status'] == 'fail'
        assert response['message'] == 'Invalid page number'

    def test_municipalities_invalid_page_size(self):
        for _ in range(40):
            add_municipality()
        params = {'size': 'large', 'page': 1}
        response, status_code = self.get_request('/api/v1/municipalities',
                                                 params=params)
        assert status_code == 422
        assert response['status'] == 'fail'
        assert response['message'] == 'Invalid page size'

    def test_get_municipality(self):
        town = add_town()
        add_trip(municipality_id=town.municipality.id, destination_id=town.id)
        response, status_code = self.get_request('/api/v1/municipalities/1')
        assert status_code == 200
        assert 'id' in response['municipality']
        assert 'name' in response['municipality']
        assert 'stateId' in response['municipality']
        assert 'cagem' in response['municipality']
        assert len(response['municipality']['towns']) == 1
        assert 'id' in response['municipality']['towns'][0]
        assert len(response['municipality']['trips']) == 1
        assert 'id' in response['municipality']['trips'][0]

    def test_get_non_existing(self):
        municipality = add_municipality()
        endpoint = '/api/v1/municipalities/%s' % (municipality.id + 3)
        response, status_code = self.get_request(endpoint)
        assert status_code == 404
        assert response['status'] == 'fail'
        assert response['message'] == 'Municipality not found'

    def test_get_invalid_id(self):
        add_municipality()
        response, status_code = self.get_request('/api/v1/municipalities/one')
        assert status_code == 422
        assert response['status'] == 'fail'
        assert response['message'] == 'Invalid id'

    def test_get_municipality_towns(self):
        add_town()
        endpoint = '/api/v1/municipalities/1/towns'
        response, status_code = self.get_request(endpoint)
        assert status_code == 200
        assert 'id' in response['towns'][0]
        assert 'name' in response['towns'][0]
        assert 'clge' in response['towns'][0]
        assert 'longitude' in response['towns'][0]
        assert 'latitude' in response['towns'][0]
        assert 'hasTrips' in response['towns'][0]

    def test_get_municipality_trips(self):
        municipality = add_municipality()
        add_trip(municipality_id=municipality.id)
        endpoint = '/api/v1/municipalities/1/trips'
        response, status_code = self.get_request(endpoint)
        assert status_code == 200
        assert 'id' in response['trips'][0]
        assert 'active' in response['trips'][0]
        assert 'finished' in response['trips'][0]
        assert 'date' in response['trips'][0]
        assert 'time' in response['trips'][0]
        assert 'destinationId' in response['trips'][0]
        assert 'organizerId' in response['trips'][0]
        assert 'size' in response['trips'][0]
        assert 'information' in response['trips'][0]
        assert 'participantsNumber' in response['trips'][0]
