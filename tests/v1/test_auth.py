import datetime
import pytz

from tests.base import BaseTestCase
from tests.utils import add_user
from unittest import mock

from soyvoluntario.models import User


class AuthTestCase(BaseTestCase):
    def test_login(self):
        add_user('test', 'test@domain.com', 'test')
        data = {
            'email': 'test@domain.com',
            'password': 'test',
        }
        response, status_code = self.post_request('/api/v1/auth/login', data)
        assert status_code == 200
        assert response['status'] == 'success'
        assert response['message'] == 'Successfully logged in'
        assert 'auth_token' in response

    def test_login_no_data(self):
        data = {}
        response, status_code = self.post_request('/api/v1/auth/login', data)
        assert status_code == 400
        assert response['status'] == 'fail'
        assert response['message'] == 'Empty request'

    def test_login_no_email(self):
        data = {
            'password': 'test',
        }
        response, status_code = self.post_request('/api/v1/auth/login', data)
        assert status_code == 400
        assert response['status'] == 'fail'
        assert response['message'] == 'Empty email'

    def test_login_no_password(self):
        data = {
            'email': 'test@domain.com',
        }
        response, status_code = self.post_request('/api/v1/auth/login', data)
        assert status_code == 400
        assert response['status'] == 'fail'
        assert response['message'] == 'Empty password'

    def test_login_invalid_email(self):
        data = {
            'email': 'test-is-not-an-email',
            'password': 'test',
        }
        response, status_code = self.post_request('/api/v1/auth/login', data)
        assert status_code == 422
        assert response['status'] == 'fail'
        assert response['message'] == 'Invalid email'

    def test_login_no_existing_user(self):
        add_user('test', 'test@domain.com', 'test')
        data = {
            'email': 'test@other-domain.com',
            'password': 'test',
        }
        response, status_code = self.post_request('/api/v1/auth/login', data)
        assert status_code == 409
        assert response['status'] == 'fail'
        assert response['message'] == 'Wrong email or password'

    def test_login_wrong_password(self):
        add_user('test', 'test@domain.com', 'test')
        data = {
            'email': 'test@other-domain.com',
            'password': 'password',
        }
        response, status_code = self.post_request('/api/v1/auth/login', data)
        assert status_code == 409
        assert response['status'] == 'fail'
        assert response['message'] == 'Wrong email or password'

    def test_valid_token(self):
        user = add_user('test', 'test@domain.com', 'test')
        data = {
            'email': 'test@domain.com',
            'password': 'test',
        }
        response, _ = self.post_request('/api/v1/auth/login', data)
        token = response['auth_token']
        decoded_token = User.decode_auth_token(token)
        assert decoded_token['id'] == user.id

    def test_invalid_token(self):
        decoded_token = User.decode_auth_token('abcd')
        assert isinstance(decoded_token, str)
        assert decoded_token == 'Invalid token'

    def test_expired_token(self):
        add_user('test', 'test@domain.com', 'test')
        data = {
            'email': 'test@domain.com',
            'password': 'test',
        }
        delta = datetime.timedelta(days=40)
        past_date = datetime.datetime.now(tz=pytz.utc) - delta
        with mock.patch('soyvoluntario.models.datetime.datetime') as mock_dt:
            mock_dt.now.return_value = past_date
            response, _ = self.post_request('/api/v1/auth/login', data)
            token = response['auth_token']
        decoded_token = User.decode_auth_token(token)
        assert isinstance(decoded_token, str)
        assert decoded_token == 'Signature expired'
