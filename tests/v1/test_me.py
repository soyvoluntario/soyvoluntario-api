from tests.base import BaseTestCase

from tests.utils import add_user, add_trip


class TestMe(BaseTestCase):
    def test_get_profile(self):
        user = add_user('Testing User', 'test@domain.com', 'test')
        token = self.get_token('test@domain.com', 'test')
        response, status_code = self.get_request('/api/v1/me', token)
        assert status_code == 200
        assert response['profile']['id'] == user.id
        assert response['profile']['email'] == user.email
        assert response['profile']['cellphone'] == user.cellphone
        assert response['profile']['twitterHandle'] == user.twitter_handle
        assert 'trips' in response['profile']
        assert 'organizedTrips' in response['profile']
        assert 'password' not in response['profile']

    def test_get_profile_visitor(self):
        response, status_code = self.get_request('/api/v1/me')
        assert status_code == 401

    def test_update_profile(self):
        user = add_user('Testing User', 'test@domain.com', 'test')
        token = self.get_token('test@domain.com', 'test')
        response, status_code = self.put_request('/api/v1/me', {}, token)
        assert response['message'] == 'Profile updated'
        assert response['profile']['id'] == user.id
        assert response['profile']['email'] == user.email
        assert response['profile']['cellphone'] == user.cellphone
        assert response['profile']['twitterHandle'] == user.twitter_handle
        assert 'trips' in response['profile']
        assert 'organizedTrips' in response['profile']
        assert 'password' not in response['profile']

    def test_update_email(self):
        add_user('Testing User', 'test@domain.com', 'test')
        data = {'email': 'newmail@domain.com'}
        token = self.get_token('test@domain.com', 'test')
        response, status_code = self.put_request('/api/v1/me', data, token)
        assert status_code == 200
        assert response['profile']['email'] == 'newmail@domain.com'

    def test_update_password(self):
        add_user('Testing User', 'test@domain.com', 'test')
        data = {'password': 'newpass'}
        token = self.get_token('test@domain.com', 'test')
        self.put_request('/api/v1/me', data, token)
        token = self.get_token('test@domain.com', 'newpass')
        response, status_code = self.get_request('/api/v1/me', token)
        assert status_code == 200

    def test_update_cellphone(self):
        add_user('Testing User', 'test@domain.com', 'test')
        data = {'cellphone': '987654321'}
        token = self.get_token('test@domain.com', 'test')
        response, status_code = self.put_request('/api/v1/me', data, token)
        assert status_code == 200
        assert response['profile']['cellphone'] == '987654321'

    def test_update_twitter_handle(self):
        add_user('Testing User', 'test@domain.com', 'test')
        data = {'twitterHandle': 'test2'}
        token = self.get_token('test@domain.com', 'test')
        response, status_code = self.put_request('/api/v1/me', data, token)
        assert status_code == 200
        assert response['profile']['twitterHandle'] == 'test2'

    def test_get_trips(self):
        user = add_user('Testing User', 'test@domain.com', 'test')
        add_trip(user.id)
        token = self.get_token('test@domain.com', 'test')
        response, status_code = self.get_request('/api/v1/me/trips', token)
        assert status_code == 200
        assert len(response['trips']) == 1
        trip = response['trips'][0]
        assert trip['id'] == 1
        assert trip['organizerId'] == user.id
        assert 'municipalityId' in trip
        assert 'destinationId' in trip
        assert trip['size'] == 3
        assert 'date' in trip
        assert 'time' in trip
        assert trip['participantsNumber'] == 1
        assert trip['active']
        assert not trip['finished']
        assert 'createdAt' in trip
        assert 'information' in trip
