import json

from flask_testing import TestCase

from soyvoluntario import create_app, db

app = create_app()


class BaseTestCase(TestCase):
    """Basic test case configuration."""
    def create_app(self):
        app.config.from_object('soyvoluntario.config.TestingConfig')
        return app

    def get_request(self, endpoint, token=None, params=None):
        headers = {}
        if token:
            headers = {
                'Authorization': 'Bearer %s' % token,
            }
        response = self.client.get(endpoint,
                                   content_type='application/json',
                                   headers=headers,
                                   query_string=params,
                                   )
        return json.loads(response.data.decode()), response.status_code

    def post_request(self, endpoint, data, token=None):
        data = json.dumps(data)
        headers = {}
        if token:
            headers = {
                'Authorization': 'Bearer %s' % token,
            }
        response = self.client.post(endpoint, data=data,
                                    content_type='application/json',
                                    headers=headers,
                                    )
        return json.loads(response.data.decode()), response.status_code

    def put_request(self, endpoint, data, token=None):
        data = json.dumps(data)
        headers = {}
        if token:
            headers = {
                'Authorization': 'Bearer %s' % token,
            }
        response = self.client.put(endpoint, data=data,
                                   content_type='application/json',
                                   headers=headers,
                                   )
        return json.loads(response.data.decode()), response.status_code

    def get_token(self, email, password):
        data = {
            'email': email,
            'password': password,
        }
        response, _ = self.post_request('/api/v1/auth/login', data)
        return response['auth_token']

    def setUp(self):
        db.create_all()
        db.session.commit()

    def tearDown(self):
        db.session.remove()
        db.drop_all()
