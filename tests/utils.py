import datetime
import random
import string

from soyvoluntario import db
from soyvoluntario.models import Municipality, State, Town, Trip, User


def random_str(n):
    return ''.join([random.choice(string.ascii_letters) for i in range(n)])


def add_state(name=None):
    name = name if name else random_str(15)
    state = State(name)
    db.session.add(state)
    db.session.commit()
    return state


def add_municipality(name=None, state_id=None):
    if not state_id:
        state = add_state(random_str(15))
        state_id = state.id
    name = name if name else random_str(15)
    municipality = Municipality(name, state_id)
    db.session.add(municipality)
    db.session.commit()
    return municipality


def add_town(name=None, municipality_id=None):
    if not municipality_id:
        municipality = add_municipality(random_str(15))
        municipality_id = municipality.id
    name = name if name else random_str(15)
    longitude = random_str(15)
    latitude = random_str(15)
    town = Town(name, municipality_id, longitude, latitude)
    db.session.add(town)
    db.session.commit()
    return town


def add_user(name, email, password, twitter_handle=None,
             created_at=datetime.datetime.utcnow()):
    user = User(name, email, password, twitter_handle, created_at)
    db.session.add(user)
    db.session.commit()
    return user


def add_trip(organizer_id=None, municipality_id=None, destination_id=None,
             size=3, date=datetime.datetime.utcnow()):
    if not organizer_id:
        email = '%s@%s.%s' % (random_str(5), random_str(5), random_str(5))
        user = add_user('test', email, 'test')
        organizer_id = user.id
    else:
        user = User.query.filter_by(id=organizer_id).first()

    if not municipality_id:
        municipality = add_municipality(random_str(15))
        municipality_id = municipality.id

    if not destination_id:
        destination = add_town(random_str(15), municipality_id)
        destination_id = destination.id

    trip = Trip(organizer_id, municipality_id, destination_id, size, date)
    trip.participants.append(user)
    db.session.add(trip)
    db.session.commit()
    return trip
