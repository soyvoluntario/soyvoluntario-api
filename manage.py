"""Manager script."""
from flask_migrate import MigrateCommand
from flask_script import Manager

from soyvoluntario import create_app, db
from soyvoluntario.models import *  # To do migrations

app = create_app()  # pylint: disable=invalid-name
manager = Manager(app)  # pylint: disable=invalid-name

manager.add_command('db', MigrateCommand)

@manager.command
def seed_towns():
    import csv
    print("Creating states")
    with open('states.csv') as csvfile:
        states = csv.DictReader(csvfile)
        for state in states:
            new_state = State(state['name'], state['cagee'])
            db.session.add(new_state)
        db.session.commit()

    print("Creating municipalities")
    with open('municipalities.csv') as csvfile:
        municipalities = csv.DictReader(csvfile)
        i = 0
        for municipality in municipalities:
            new_municipality = Municipality(municipality['name'],
                                            municipality['cagee'],
                                            municipality['cagem'])
            db.session.add(new_municipality)
            i += 1
            if i % 500 == 0:
                print(i)
                db.session.commit()

        db.session.commit()

    print("Creating towns")
    with open('towns.csv') as csvfile:
        towns = csv.DictReader(csvfile)
        i = 0
        prev_cagee = 0
        prev_cagem = 0
        for town in towns:
            if prev_cagee != town['cagee'] or prev_cagem != town['cagem']:
                prev_cagee = town['cagee']
                prev_cagem = town['cagem']
                municipality = Municipality.query.filter_by(
                    state_id=town['cagee']).filter_by(
                    cagem=town['cagem']).first()
            new_town = Town(town['name'], municipality.id,
                            town['longitude'], town['latitude'],
                            town['clge'])
            db.session.add(new_town)
            i += 1
            if i % 5000 == 0:
                print(i)
                db.session.commit()

        db.session.commit()

if __name__ == '__main__':
    manager.run()
